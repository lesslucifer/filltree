﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using csv2json.convert.objConverter;
using System.Data;

namespace PGConfigConverter.convert.objConverter
{
    class GameConfig : BaseConverter
    {
        public override Dictionary<string, object> convert(string path)
        {
            base.convert(path);

            DataTable table = data[getSheetName("Game")];
            Dictionary<string, object> game = new Dictionary<string, object>();
            List<object> levels = new List<object>();

            List<int> levelIndices = this.inemptyIndices(table, 0, 0, table.Rows.Count);
            for (int i = 0; i < levelIndices.Count - 1; ++i)
            {
                Dictionary<string, object> level = new Dictionary<string,object>();
                int beg = levelIndices[i];
                int end = levelIndices[i + 1];

                int lvCol = 1;

                Dictionary<string, object> reqs = this.sub(level, "Reqs");
                this.put(reqs, "maxMoves", Convert.ToInt32(table.Rows[beg][lvCol++]));

                Dictionary<string, object> res = this.sub(reqs, "resources");
                Dictionary<string, object> gameRate = this.sub(level, "rate");
                Dictionary<string, object> rate = this.sub(gameRate, "rate");

                for (int j = beg; j < end; ++j)
                {
                    int reqCol = lvCol;

                    string corpes = table.Rows[j][reqCol++].ToString();
                    if (!string.IsNullOrEmpty(corpes))
                    {
                        this.put(res, corpes, Convert.ToInt32(table.Rows[j][reqCol++]));
                        this.put(rate, corpes, Convert.ToDouble(table.Rows[j][reqCol++]));
                    }
                }
                lvCol += 3;

                List<object> stars = new List<object>();
                for (int j = beg; j < beg + 3; ++j)
                {
                    stars.Add(Convert.ToInt32(table.Rows[j][lvCol]));
                }
                this.put(gameRate, "star", stars);

                ++lvCol;
                Dictionary<string, object> gen = this.sub(level, "Generator");
                this.put(gen, "type", table.Rows[beg][lvCol++].ToString());
                Dictionary<string, object> genData = this.sub(gen, "data");
                for (int j = beg; j < end; ++j)
                {
                    string genDataField = table.Rows[j][lvCol].ToString();
                    object genDataVal = table.Rows[j][lvCol + 1];
                    if (!string.IsNullOrEmpty(genDataField))
                    {
                        this.putField(genData, genDataField, genDataVal);
                    }
                }
                lvCol += 2;

                Dictionary<string, object> board = this.sub(level, "Board");
                Dictionary<string, object> boardSz = this.sub(board, "boardsize");
                int nrows = Convert.ToInt32(table.Rows[beg][lvCol++]);
                int ncols = Convert.ToInt32(table.Rows[beg][lvCol++]);
                this.put(boardSz, "row", nrows);
                this.put(boardSz, "col", ncols);

                string boardSheet = table.Rows[beg][lvCol++].ToString();
                if (!string.IsNullOrEmpty(boardSheet))
                {
                    BoardConverter boardConverter = new BoardConverter(boardSheet, (uint) nrows, (uint) ncols);
                    this.put(board, "cells", boardConverter.convert(Program.getInFilePath("Levels")));
                }
                else
                {
                    this.sub(board, "cells");
                }

                Dictionary<string, object> cellSz = this.sub(level, "CellSize");
                this.put(cellSz, "width", Convert.ToDouble(table.Rows[beg][lvCol++]));
                this.put(cellSz, "height", Convert.ToDouble(table.Rows[beg][lvCol++]));

                levels.Add(level);
            }

            this.put(game, "Levels", levels);
            return game;
        }
    }
}
