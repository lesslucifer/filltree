﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using PGConfigConverter.convert.objConverter;

namespace csv2json.convert.objConverter
{
    public abstract class BaseConverter : IObjectConverter
    {
        protected Dictionary<string, DataTable> data = new Dictionary<string, DataTable>();
        private string connstring;

        public BaseConverter()
        {
        }

        private void buildPath(string mainPath)
        {
            connstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + mainPath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";  
        }

        protected void readFromDB()
        {
            using (OleDbConnection conn = new OleDbConnection(connstring))
            {
                conn.Open();

                //Get All Sheets Name
                DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });

                for (int i = 0; i < sheetsName.Rows.Count; ++i)
                {
                    string sheetName = sheetsName.Rows[i][2].ToString();
                    DataTable sheetData = new DataTable();

                    string sql = string.Format("SELECT * FROM [{0}]", sheetName);
                    OleDbDataAdapter ada = new OleDbDataAdapter(sql, connstring);
                    ada.Fill(sheetData);

                    data.Add(sheetName, sheetData);
                }
            }
        }

        protected string getSheetName(string sheetName)
        {
            if (sheetName.Contains('-') || sheetName.Contains('.') || sheetName.Contains(' '))
            {
                return string.Format("'{0}$'", sheetName);
            }

            return string.Format("{0}$", sheetName);
        }

        protected void put(Dictionary<string, object> d, string k, object v)
        {
            if (d.ContainsKey(k))
            {
                d[k] = v;
            }
            else
            {
                d.Add(k, v);
            }
        }

        protected Dictionary<string, object> sub(Dictionary<string, object> d, string k)
        {
            if (!d.ContainsKey(k))
            {
                this.put(d, k, new Dictionary<string, object>());
            }

            return (Dictionary<string, object>)d[k];
        }

        public virtual Dictionary<string, object> convert(string path)
        {
            buildPath(path);
            this.readFromDB();

            return null;
        }

        protected void putField(Dictionary<string, object> parent, string field, object val)
        {
            if (!string.IsNullOrEmpty(field) && !string.IsNullOrEmpty(val.ToString()))
            {
                string[] needTree = field.Split('.');
                Dictionary<string, object> cNode = parent;
                for (int i = 0; i < needTree.Length - 1; ++i)
                {
                    string needNode = needTree[i];
                    cNode = this.sub(cNode, needNode);
                }
                string lastNeedField = needTree[needTree.Length - 1];
                this.put(cNode, lastNeedField, val);
            }
        }

        protected void append(Dictionary<string, object> d, object v)
        {
            this.put(d, d.Count.ToString(), v);
        }

        protected List<int> inemptyIndices(DataTable table, int col, int beg, int end)
        {
            List<int> indices = new List<int>();
            List<object> vals = new List<object>();
            for (int i = beg; i < end; ++i)
            {
                string data = table.Rows[i][col].ToString();
                if (!string.IsNullOrEmpty(data) && !vals.Contains(data))
                {
                    indices.Add(i);
                    vals.Add(data);
                }
            }
            indices.Add(end);

            return indices;
        }
    }
}
