﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGConfigConverter.convert.objConverter
{
    interface IObjectConverter
    {
        Dictionary<string, object> convert(string path);
    }
}
