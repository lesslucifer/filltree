﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using csv2json.convert.objConverter;
using Newtonsoft.Json;
using PGConfigConverter.convert.objConverter;

namespace PGConfigConverter
{
    class Program
    {
        private static string[] files = new string[] {"Game"};
        private static IObjectConverter[] converters = new IObjectConverter[] {new GameConfig()};

        static void Main(string[] args)
        {
            // Get all file
            string currentDirectory = Directory.GetCurrentDirectory();
            string parent = "..\\..";

            for (int i = 0; i < files.Length; ++i)
            {
                try
                {
                    string inFilePath = parent + "\\Design\\xls\\" + files[i] + ".xls";
                    string outFilePath = parent + "\\Resources\\" + files[i] + ".data";
                    IObjectConverter converter = converters[i];

                    Console.WriteLine("Starting convert file: {0}", inFilePath);

                    Dictionary<string, object> jsonContent = converter.convert(inFilePath);
                    string jsonString = JsonConvert.SerializeObject(jsonContent);

                    Console.WriteLine("Write converted data to: {0}", inFilePath);

                    StreamWriter outStream = new StreamWriter(outFilePath);
                    outStream.Write(jsonString);
                    outStream.Close();

                    Console.WriteLine("Convert file: {0} completed!", inFilePath);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine("Error: {0}", ex);
                }
            }

            Console.WriteLine("Completed!!!");
            Console.ReadLine();

        }

        public static string getInFilePath(string fileName)
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string parent = "..\\..";
            return parent + "\\Design\\xls\\" + fileName + ".xls";
        }
    }
}
