#pragma once
#include "cocos2d.h"
#include "ccHelp.h"

USING_NS_CC;
USING_CC_HELP;

namespace FillTree
{
	namespace UIConfig
	{
		class GameDialog
		{
			SYTHESINE_VAR_GET(Size, DialogSize);
			SYTHESINE_VAR_GET(string, Background);

			SYTHESINE_VAR_GET(string, DlgButtonBgNormal);
			SYTHESINE_VAR_GET(string, DlgButtonBgSelected);
			SYTHESINE_VAR_GET(string, DlgButtonFont);
			SYTHESINE_VAR_GET(float, DlgButtonFontSize);
			SYTHESINE_VAR_GET(Vec2, DlgButtonPos);

			SYTHESINE_VAR_GET(Size, DlgCloseButtonSize);
			SYTHESINE_VAR_GET(Vec2, DlgCloseButtonPos);
			SYTHESINE_VAR_GET(string, DlgCloseButtonImg);

		protected:
			virtual void androidInit();

		public:
			static GameDialog AndroidConfig;

		private:
			STATIC_CONSTRUCTOR_DECLARE(GameDialog);
		};
	}
}