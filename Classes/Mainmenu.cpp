#include "Mainmenu.h"
#include "ui/CocosGUI.h"
#include "MatchScene.h"
#include "SelectLevels.h"
#include "ccHelp.h"
#include "Global.h"

USING_NS_CC;
using namespace ui;

namespace FillTree
{
	bool MainmenuScene::init()
	{
		if (!Layer::init())
		{
			return false;
		}

		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		Vec2 center = (origin + visibleSize) / 2;

		auto background = Sprite::create("mainmenu_background.png");
		background->setPosition(center);
		this->addChild(background);

		auto playButton = Button::create("button_normal.png", "button_selected.png");
		playButton->addTouchEventListener([]
			(Ref *obj, Widget::TouchEventType _event)
			{
				if (_event == Widget::TouchEventType::ENDED)
				{
					auto selectLevels = Utils::createScene<SelectLevelScene>();
					Director::getInstance()->replaceScene(selectLevels);
				}
			});

		playButton->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		playButton->setPosition(Vec2(center.x, 500));
		playButton->setTitleFontName("Cooper Std Black");
		playButton->setTitleFontSize(60);
		playButton->setTitleText("Play");
		playButton->setTitleColor(Color3B::WHITE);
		this->addChild(playButton);

		return true;
	}
}
