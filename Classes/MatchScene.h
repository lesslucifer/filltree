#ifndef __MATCH_SCENE_H__
#define __MATCH_SCENE_H__
#include "cocos2d.h"
#include "ccHelp.h"
#include "BoardController.h"
#include "MatchSceneUINode.h"
#include "BoardView.h"
#include "GameConfig.h"

USING_CC_HELP;

namespace FillTree
{
	class MatchScene : public cocos2d::Layer, public MatchDataDelegate
	{
	public:
		MatchScene(CREF(LevelConfig) _config, uint lvlIndex);
		virtual bool init();

		void pointChanged(CellType color, uint newPoint) override;
		void movesChanged(uint newMove) override;
		void gameEnded(const MatchData *data) override;

	private:
		uint levelIndex;
		const LevelConfig &config;
		RefHolder<MatchData> matchData;
		RefHolder<Board> board;
		RefHolder<BoardController> boardController;
		MatchSceneUINode *uiNode;
		BoardView *boardView;
        
        ccHelp::OperationManager mOP;

		void backToSelectLevels();
		void tryToNextScene();
		void replayMatch();
		void saveGameData();

		void closeButtonTouched();
        
	private:
		void capture(string savedFile);
	};
}
#endif
