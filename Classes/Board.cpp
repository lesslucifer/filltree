#include "Board.h"
#include "Cells.h"
#include "Global.h"
#include <algorithm>

USING_NS_CC;

namespace FillTree
{
	typedef hmap<Index2D, uint> GenerateLevel;
	class BoardMoveCollector
	{
	public:
		BoardMoveCollector(const Board *board) : board(board) {}

		void collectMove(BoardMove& move)
		{
			Index2D start = move.front();
			auto startCell = board->get(start);
			if (CellType::Generator == startCell->getData().getType())
			{
				for (uint i = 0; i < generated[start]; ++i)
				{
					move.insert(move.begin(), Index2D(start.row + i + 1, start.col));
				}
				++generated[start];
			}

			moves.push_back(move);
		}

	private:
		BoardMoveList moves;
		GenerateLevel generated;

		const Board *board;

	public:
		PROPERTY_GET(CREF(BoardMoveList), Moves, moves);
		PROPERTY_GET(CREF(GenerateLevel), Generated, generated);
	};
	
	Board::Board(CREF(BoardTemplate) config, RefHolder<CellGenerator> rand,
		BoardTransformListener *_del)
		: Array2D<Cell>(config.getBoardSize().row, config.getBoardSize().col),
		_delegate(_del), generator(rand), extractor(*this),
		backgrounds(config.getBoardSize().row, config.getBoardSize().col)
	{
		loadBoard(config);
		if (_delegate)
		{
			_delegate->boardDidLoad(*this);
		}
	}

	void Board::loadBoard(CREF(BoardTemplate) config)
	{
		srand(time(NULL));
		bool succ = false;
		do 
		{
			Index2D idx;
			for (idx.row = 0; idx.row < nRows(); ++idx.row)
			{
				for (idx.col = 0; idx.col < nCols(); ++idx.col)
				{
					CellData cellData = config[idx];
					RefHolder<Cell> cell = nullptr;

					if (CellType::Empty == cellData.getType())
					{
						cell = this->generator->nextCell(idx);
					}
					else
					{
						cell = Cells::create(cellData);
					}
					this->put(cell(), idx.row, idx.col);
				}
			}

			succ = this->extractor.checkBoard();
		} while (!succ);
	}

	Cellbackground& Board::getCellbackground(CREF(Index2D) idx)
	{
		return backgrounds[idx];
	}

	const Cellbackground& Board::getCellbackground(CREF(Index2D) idx) const
	{
		return backgrounds[idx];
	}
	
	void Board::arrange(BoardMoveCollector &collector)
	{
		Index2D idx;
		while (idx.row < this->nRows())
		{
			Cell *cell = this->get(idx);
			if (CellType::Empty == cell->getData().getType())
			{
				BoardMove move;
				bool foundPath = this->findPath(idx, move);
				
				if (foundPath)
				{
					// replace internal
					Index2D start = move.front();
					Index2D end = move.back();

					Cell *startCell = this->get(start);
					if (CellType::Generator != startCell->getData().getType())
					{
						this->put(startCell, idx);
						this->put(Cells::empty(), start);
					}
					else
					{
						this->put(generator->nextCell(idx)(), idx);
					}

					collector.collectMove(move);
				}
			}

			++idx.col;
			if (idx.col >= this->nCols())
			{
				idx.col = 0;
				++idx.row;
			}
		}
	}

	bool Board::findPath(CREF(Index2D) target, BoardMove &tracer) const
	{
		const Cell *cell = this->get(target);
		if (!cell)
		{
			return false;
		}

		tracer.push_front(target);
		if (CellType::Empty == cell->getData().getType())
		{
			if (!(findPath(Index2D(target.row + 1, target.col), tracer) ||
			 findPath(Index2D(target.row + 1, target.col - 1), tracer) ||
			 findPath(Index2D(target.row + 1, target.col + 1), tracer)))
			{
				tracer.pop_front();
				return false;
			}
		}
		else if (cell->isStatic())
		{
			tracer.pop_front();
			return false;
		}
		
		return true;
	}

	void Board::arrange()
	{
		CCASSERT(_delegate, "Board delegate must not be null");

		BoardMoveCollector collector(this);
		this->arrange(collector);
        
        _delegate->cellsDidMove(*this, collector.getMoves());
	}

	void Board::clearAllBonus(CREF(list<Index2D>) excepts)
    {
        std::set<Index2D> setIdxs(excepts.cbegin(), excepts.cend());
        Index2D idx;
        for (idx.row = 0; idx.row < nRows(); ++idx.row)
        {
            for (idx.col = 0; idx.col < nCols(); ++idx.col)
            {
                if (this->backgrounds[idx].bonus() > 0 &&
                    setIdxs.find(idx) == setIdxs.cend())
                {
                    this->backgrounds[idx].bonus = 0;
                }
            }
        }
	}

	void Board::giveBonus(CREF(list<Index2D>) idxs)
	{
        for (CREF(auto) idx : idxs)
        {
            if (backgrounds[idx].bonus() < 10)
            {
                backgrounds[idx].bonus = backgrounds[idx].bonus() + 1;
            }
        }
	}

	GameResources Board::calcScore(CREF(list<Index2D>) idxs) const
	{
		GameResources score;
		for (auto &idx : idxs)
		{
			score[(*this)[idx]->getData().getType()] += 1 +
				this->backgrounds[idx].bonus();
		}

		return score;
	}

	void Board::activeCells(CREF(list<Index2D>) indices)
	{
		CCASSERT(_delegate, "Board delegate must not be null");

		for (CREF(Index2D) idx : indices)
		{
			this->put(Cells::empty(), idx);
		}
        
        _delegate->cellsDidActive(*this, indices);
	}

	void Board::killBoard()
	{
		CCASSERT(_delegate, "Board delegate must not be null");
        _delegate->boardDidKilled();
	}

	void Board::shuffle()
	{
		CCASSERT(_delegate, "Board delegate must not be null");

		vector<Index2D> selectedIndices;
		list<RefHolder<Cell>> selectedCells;

		Index2D idx;
		for (idx.row = 0; idx.row < nRows(); ++idx.row)
		{
			for (idx.col = 0; idx.col < nCols(); ++idx.col)
			{
				if (this->get(idx)->isCollectable())
				{
					selectedIndices.push_back(idx);
					selectedCells.push_back(this->get(idx));
				}
			}
		}

		vector<Index2D> shuffledIndices(selectedIndices);
		bool hasMatch = false;
		do
		{
			// clear board
			for (CREF(auto) idx : selectedIndices)
			{
				this->put(nullptr, idx);
			}

			// shuffle board
			std::random_shuffle(shuffledIndices.begin(), shuffledIndices.end());

			// re-fill
			int i = 0;
			for (auto cell : selectedCells)
			{
				this->put(cell(), shuffledIndices[i++]);
			}

			hasMatch = extractor.checkBoard();
		} while (!hasMatch);

		int i = 0;
		BoardMoveList moves;
		for (auto cell : selectedCells)
		{
			BoardMove move;
			move.push_back(selectedIndices[i]);
			move.push_back(shuffledIndices[i]);
			moves.push_back(move);

			this->put(cell(), shuffledIndices[i]);
			++i;
		}
        
        _delegate->boardDidShuffle();
        _delegate->cellsDidMove(*this, moves);
	}

	const BoardExtractor& Board::getExtractor() const
	{
		return this->extractor;
	}

	BoardExtractor& Board::getExtractor()
	{
		return this->extractor;
	}

	BoardExtractor::BoardExtractor(CREF(Board) board)
		: board(board) {}

	void BoardExtractor::floodFill(CREF(Index2D) idx,
		_out_ list<Index2D>& cells) const
	{
		if (!board.isContains(idx) || !board[idx]->isCollectable())
			return;

		bool **visited = alloc2D<bool>(this->board.nRows(), this->board.nCols(), false);
		auto filter = Global::getFilterByType(board[idx]->getData().getType());

		this->floodFill(idx, filter, cells, visited);
		delete2D(visited, this->board.nRows());
	}

	bool BoardExtractor::checkBoard() const
	{
		uint rows = board.nRows();
		uint cols = board.nCols();

		bool ret = false;
		bool **visited = alloc2D<bool>(rows, cols, false);
		bool **extracted = alloc2D<bool>(rows, cols, false);

		Index2D idx;
		for (idx.row = 0; idx.row < rows; ++idx.row)
		{
			for (idx.col = 0; idx.col < cols; ++idx.col)
			{
				auto cell = board[idx];
				if (!extracted[idx.row][idx.col] && cell->isCollectable())
				{
					list<Index2D> cells;
					CellType type = cell->getData().getType();
					auto filter = [=](CREF(Cell) cell, CREF(Index2D) index) -> bool
					{
						return cell.getData().getType() == type;
					};
					set2D(visited, rows, cols, false);
					floodFill(idx, filter, cells, visited);

					ret = Global::MATCH_RULE(cells);
					if (ret) break;

					for (CREF(Index2D) cell : cells)
					{
						extracted[cell.row][cell.col] = true;
					}
				}
			}

			if (ret) break;
		}

		delete2D(visited, rows);
		delete2D(extracted, rows);

		return ret;
	}

	void BoardExtractor::floodFill(CREF(Index2D) idx,
		const function<bool(CREF(Cell), CREF(Index2D))>& filter,
		_out_ list<Index2D>& cells,
		bool** visited) const
	{
		if (this->board.isContains(idx))
		{
			bool isVisited = visited[idx.row][idx.col];
			visited[idx.row][idx.col] = true;

			if (!isVisited && filter(*board[idx], idx))
			{
				cells.push_back(idx);

				this->floodFill(Index2D(idx.row + 1, idx.col), filter, cells, visited);
				this->floodFill(Index2D(idx.row - 1, idx.col), filter, cells, visited);
				this->floodFill(Index2D(idx.row, idx.col + 1), filter, cells, visited);
				this->floodFill(Index2D(idx.row, idx.col - 1), filter, cells, visited);
			}
		}
	}
}
