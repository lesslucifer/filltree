#include "GameRating.h"

namespace FillTree
{
	uint GameRating::rate(const GameResources &cropsies) const
	{
		float point = 0;
		for (auto req : corpesRate)
		{
			if (cropsies.find(req.first) != cropsies.cend())
			{
				point += req.second * cropsies.at(req.first);
			}
		}

		return static_cast<uint>(point);
	}

	uint GameRating::rateStar(uint point) const
	{
		for (uint i = 0; i < this->star.size(); ++i)
		{
			if (star[i] > point)
			{
				return i;
			}
		}

		return star.size();
	}
}