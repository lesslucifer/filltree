#include "Cells.h"
#include <set>

namespace FillTree
{
	namespace Cells
	{
		CellType cropsies[] =
		{
			CellType::Red, CellType::Green,
			CellType::Blue, CellType::Orange,
			CellType::Purple, CellType::Yellow
		};
		const vector<CellType> CropseyTypes(std::begin(cropsies), std::end(cropsies));
        const std::set<CellType> _CropseySet(std::begin(cropsies), std::end(cropsies));

		bool isCropsey(CellType type)
		{
			return _CropseySet.find(type) != _CropseySet.cend();
		}

		Cell* create(CREF(CellData) conf)
		{
			if (conf.getType() == CellType::Empty)
			{
				return empty();
			}
			else if (conf.getType() == CellType::Generator)
			{
				return generator();
			}
			else if (conf.getType() == CellType::Pad)
			{
				return pad();
			}
			else if (isCropsey(conf.getType()))
			{
				return getCropsey(conf);
			}

			CCASSERT(false, "invalid cel type");
			return nullptr;
		}
	}
}