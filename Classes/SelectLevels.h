#pragma once
#include "cocos2d.h"
#include "ccHelp.h"
#include <functional>

USING_NS_CC;

namespace FillTree
{
	typedef const std::function<void(uint)> LevelSelectedCallback;
	class SelectLevelScene : public Layer
	{
	private:
	public:
		virtual bool init();
		CREATE_FUNC(SelectLevelScene);

		void onEnter() override;
	private:
		void onTileSelected(uint index);
		std::list<Node *> tiles;
	};
}