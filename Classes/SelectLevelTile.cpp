#include "SelectLevelTile.h"
#include "ccHelp.h"
#include "ui/cocosGUI.h"

USING_NS_CC;
using namespace ui;
USING_CC_HELP;

namespace FillTree
{
	SelectLevelTile::SelectLevelTile(CREF(UIConfig::SelectLevel::Tile) conf,
		uint levelIndex, uint star, LevelMode mode, LevelSelectedCallback callback)
		: index(levelIndex), callback(callback)
	{
		this->setContentSize(conf.getTileSize());

		string bg = (LevelMode::Locked == mode)?conf.getLockedBackground():
			((star >= conf.getStars().size())?conf.getPerfectBackground():
			conf.getNormalBackground());
		auto tile = Button::create(bg);
		tile->setSize(conf.getTileSize());
		tile->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		this->addChild(tile);
		tile->addTouchEventListener([this]
		(Ref *obj, Widget::TouchEventType _event)
		{
			if (_event == Widget::TouchEventType::ENDED)
			{
				if (this->callback)
				{
					this->callback(this->index);
				}
			}
		});

		if (mode != LevelMode::Locked)
		{
			for (uint i = 0; i < conf.getStars().size(); ++i)
			{
				Node *starNode = nullptr;
				if (i < star)
				{
					starNode = Utils::warpSpriteBySize(conf.getSuccesStar(), conf.getStarSize());
				}
				else
				{
					starNode = Utils::warpSpriteBySize(conf.getFailedStar(), conf.getStarSize());
				}

				starNode->setPosition(conf.getStars()[i].getPosition());
				starNode->setRotation(conf.getStars()[i].getRotation());

				this->addChild(starNode);
			}

			Label *indexLbl = Label
				::createWithTTF(StringUtils::format("%d", levelIndex + 1),
				conf.getFont(), conf.getFontSize(), Size::ZERO, TextHAlignment::CENTER, TextVAlignment::BOTTOM);
			indexLbl->setTextColor(Color4B::BLACK);
			indexLbl->setPosition(conf.getFontPosition());
			this->addChild(indexLbl);
		}
	}
}