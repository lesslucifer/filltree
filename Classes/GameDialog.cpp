#include "GameDialog.h"

namespace FillTree
{
	GameDialog::GameDialog(CREF(UIConfig::GameDialog) config)
	{
		this->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		this->setContentSize(config.getDialogSize());

		auto bg = Utils::warpSpriteBySize(config.getBackground(),
			config.getDialogSize());
		bg->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		this->addChild(bg);

		auto dlgButton = ui::Button::create(config.getDlgButtonBgNormal(),
			config.getDlgButtonBgSelected());
		dlgButton->setPosition(config.getDlgButtonPos());
		dlgButton->setTitleFontName(config.getDlgButtonFont());
		dlgButton->setTitleFontSize(config.getDlgButtonFontSize());
		dlgButton->addTouchEventListener(makeCallback(GameDialogResult::OK));
		this->addChild(dlgButton);
		this->dialogButton = dlgButton;

		auto dlgCloseButton = ui::Button::create(config.getDlgCloseButtonImg());
		dlgCloseButton->setPosition(config.getDlgCloseButtonPos());
		dlgCloseButton->setSize(config.getDlgCloseButtonSize());
		dlgCloseButton->addTouchEventListener(makeCallback(GameDialogResult::Close));
		this->addChild(dlgCloseButton);
		this->closeButton = dlgCloseButton;
	}

	function<void(Ref*, ui::Widget::TouchEventType)> GameDialog::makeCallback(
		GameDialogResult result)
	{
		return [=]
		(Ref *o, ui::Widget::TouchEventType e)
		{
			if (!tapLocker.isLocked() && e == ui::Widget::TouchEventType::ENDED)
			{
				this->onCallback(result);
			}
		};
	}

	void GameDialog::onCallback(GameDialogResult result)
	{
		if (dlgCallback)
		{
			tapLocker.lock();
			dlgCallback(result);
		}
	}
}