#include "UIHeaderBarConfig.h"

namespace FillTree
{
	namespace UIConfig
	{
		HeaderBar HeaderBar::AndroidUIConfig;
		STATIC_CONSTRUCTOR_IMPL(HeaderBar)
		{
			HeaderBar::AndroidUIConfig.androidInit();
		}

		void HeaderBar::androidInit()
		{
			this->ScoreBarSize = Size(720, 280);
			this->ScoreBarPos = Vec2(0, 1000);
			this->ScorBarAnchor = Vec2::ANCHOR_BOTTOM_LEFT; 

			{ // in score bar
				this->MovesCellSize = Size(151, 112);
				this->MovesCellPos = Vec2(2, 156);

				this->MovesBackground = "hd_bar_moves.png";
				this->MovesBackgroundSize = this->MovesCellSize;
				this->MovesBackgroundPos = Vec2::ZERO;

				this->MovesFont = "fonts/CooperBlackStd.otf";
				this->MovesFontSize = 44;
				this->MovesTextPos = Vec2(75, 30);

				this->ReqBarBackground = "hdbar_scorebar_bg.png";
				this->ReqBarBackgroundSize = Size(518, 124);
				this->ReqBarBackgroundPos = Vec2(198, 152);

				this->ReqCellBeginPos = Vec2(17, 14);
				this->ReqCellDistance = 130;

				this->ReqCellBackground = "hdbar_scorecell_bg.png";
				this->ReqCellBackgroundSize = Size(96, 91);

				this->ReqIconSize = Size(45, 45);
				this->ReqIconPos = Vec2(26, 42);

				this->SuccessTick = "header_bar_req_success_tick.png";
				this->SuccessTickSize = Size(20, 20);
				this->SuccessTickPos = Vec2(25, 0);

				this->ReqFontFile = "fonts/CooperBlackStd.otf";
				this->ReqFontSize = 22;
				this->ReqTextPos = Vec2(44, 12);
			} // end in score bar

			this->ProgressPosition = Vec2(155, 156);
			{ // progress column
				this->ProgressCol.ColumnBgSize = Size(29, 118);
				this->ProgressCol.ColumnProgressSize = Size(25, 114);
				this->ProgressCol.Background = "progress_col_bg.png";
				this->ProgressCol.ProgressBg = "progress_col_inside.png";

				this->ProgressCol.ProgressPos = Vec2(2, 2);

				this->ProgressCol.StarSize = Size(27, 29);
				this->ProgressCol.ActiveStar = "progress_col_active_star.png";
				this->ProgressCol.InactiveStar = "progress_col_inactive_star.png";

				this->ProgressCol.StarPositions.push_back(Vec2(30, 20));
				this->ProgressCol.StarPositions.push_back(Vec2(30, 49));
				this->ProgressCol.StarPositions.push_back(Vec2(30, 85));
			}
		}
	}
}