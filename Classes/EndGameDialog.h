#pragma once
#include "GameDialog.h"
#include "UIEndGameDialogConfig.h"

USING_NS_CC;

namespace FillTree
{
	class EndGameDialog : public GameDialog
	{
	public:
		EndGameDialog(MatchData *data, CREF(UIConfig::EndGameDialog) config);
	};
}