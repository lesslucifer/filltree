#include "UIShuffleLayerConfig.h"

namespace FillTree
{
	namespace UIConfig
	{
		ShuffleLayer ShuffleLayer::AndroidConfig;
		STATIC_CONSTRUCTOR_IMPL(ShuffleLayer)
		{
			ShuffleLayer::AndroidConfig.androidInit();
		}
		void ShuffleLayer::androidInit()
		{
			this->TextSize = Size(270, 64);
			this->TextImage = "shuffle_text.png";

			this->LoadingSize = Size(440, 440);
			this->LoadingImage = "shuffle_loading.png";
		}
	}
}