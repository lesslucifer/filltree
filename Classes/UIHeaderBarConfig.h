#pragma once
#include "cocos2d.h"
#include "ccHelp.h"

USING_NS_CC;
USING_CC_HELP;

namespace FillTree
{
	namespace UIConfig
	{
		class HeaderBar
		{
			SYTHESINE_VAR_GET(Size, ScoreBarSize);
			SYTHESINE_VAR_GET(Vec2, ScoreBarPos);
			SYTHESINE_VAR_GET(Vec2, ScorBarAnchor);

			SYTHESINE_VAR_GET(Size, MovesCellSize);
			SYTHESINE_VAR_GET(Vec2, MovesCellPos);

			SYTHESINE_VAR_GET(string, MovesBackground);
			SYTHESINE_VAR_GET(Size, MovesBackgroundSize);
			SYTHESINE_VAR_GET(Vec2, MovesBackgroundPos);

			SYTHESINE_VAR_GET(string, MovesFont);
			SYTHESINE_VAR_GET(float, MovesFontSize);
			SYTHESINE_VAR_GET(Vec2, MovesTextPos);

			SYTHESINE_VAR_GET(string, ReqBarBackground);
			SYTHESINE_VAR_GET(Size, ReqBarBackgroundSize);
			SYTHESINE_VAR_GET(Vec2, ReqBarBackgroundPos);

			SYTHESINE_VAR_GET(Vec2, ReqCellBeginPos);
			SYTHESINE_VAR_GET(float, ReqCellDistance);

			SYTHESINE_VAR_GET(string, ReqCellBackground);
			SYTHESINE_VAR_GET(Size, ReqCellBackgroundSize);

			SYTHESINE_VAR_GET(Size, ReqIconSize);
			SYTHESINE_VAR_GET(Vec2, ReqIconPos);

			SYTHESINE_VAR_GET(string, SuccessTick);
			SYTHESINE_VAR_GET(Size, SuccessTickSize);
			SYTHESINE_VAR_GET(Vec2, SuccessTickPos);

			SYTHESINE_VAR_GET(string, ReqFontFile);
			SYTHESINE_VAR_GET(float, ReqFontSize);
			SYTHESINE_VAR_GET(Vec2, ReqTextPos);

			class GameProgressionColumn
			{
				SYTHESINE_VAR_GET(Size, ColumnBgSize);
				SYTHESINE_VAR_GET(Size, ColumnProgressSize);
				SYTHESINE_VAR_GET(string, Background);
				SYTHESINE_VAR_GET(string, ProgressBg);
				SYTHESINE_VAR_GET(Vec2, ProgressPos);

				SYTHESINE_VAR_GET(Size, StarSize);
				SYTHESINE_VAR_GET(string, InactiveStar);
				SYTHESINE_VAR_GET(string, ActiveStar);
				SYTHESINE_VAR_GET(vector<Vec2>, StarPositions);

				friend class HeaderBar;
			};

			SYTHESINE_VAR_GET(Vec2, ProgressPosition);
			SYTHESINE_VAR_GET(GameProgressionColumn, ProgressCol);

		private:
			void androidInit();

		public:
			static HeaderBar AndroidUIConfig;

			STATIC_CONSTRUCTOR_DECLARE(HeaderBar);
		};
	}
}