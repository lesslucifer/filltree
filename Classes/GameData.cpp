#include "GameData.h"

namespace FillTree
{
	void GameData::completeLevel(uint lvl, uint point, uint star)
	{
		if (point > 0 && star > 0)
		{
			LevelMode mode = getMode(lvl);
			if (LevelMode::Passed == mode &&
				point >= this->levels[lvl].getPoint())
			{
				this->levels[lvl] = GameLevelData(point, star);
			}
			else if (LevelMode::New == mode)
			{
				this->levels.push_back(GameLevelData(point, star));
			}
		}
	}

	uint GameData::getStar(uint level)
	{
		if (level >= this->levels.size())
		{
			return 0;
		}

		return this->levels[level].getStar();
	}

	LevelMode GameData::getMode(uint level)
	{
		return (level > this->levels.size())?LevelMode::Locked:
			(level == this->levels.size())?LevelMode::New:LevelMode::Passed;
	}
}