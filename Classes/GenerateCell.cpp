#include "StdCell.h"

namespace FillTree
{
	namespace Cells
	{
		class GeneratorCell : public StdCell
		{
		public:
			static RefHolder<GeneratorCell> CELL;

			CellDelegate* setDelegate(CellDelegate *del)
			{
				return nullptr;
			}

		private:
			GeneratorCell() :
				StdCell(CellType::Generator, false, false) {}
		};

		RefHolder<GeneratorCell> GeneratorCell::CELL =
			Utils::createnx(new GeneratorCell());
		Cell* generator() {return GeneratorCell::CELL();}
	}
}