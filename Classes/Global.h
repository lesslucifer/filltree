#pragma once
#include "GameData.h"
#include "ccHelp.h"
#include <list>
#include <functional>

using namespace std;

namespace FillTree
{
	class Global
	{
	public:
		static string GameDataFile;

		static void loadGameData();
		static void saveGameData();

		static GameData& getGameData();

		static const std::function<bool(CREF(list<Index2D>))> MATCH_RULE;
		static std::function<bool(CREF(Cell), CREF(Index2D))>
			getFilterByType(CellType type);
	private:
		static GameData gameData;
	};
}