#pragma once
#include "Cell.h"
#include "CellData.h"
#include "CellActiveMode.h"
#include <set>

namespace FillTree
{
	namespace Cells
	{
		extern const vector<CellType> CropseyTypes;
		bool isCropsey(CellType type);
		Cell* getCropsey(CREF(CellData) type);
		Cell* empty();
		Cell* generator();
		Cell* pad();

		Cell* create(CREF(CellData) conf);
	}
}