#pragma once
#include "cocos2d.h"
#include "UIGameDialogConfig.h"
#include "MatchData.h"
#include "ui/CocosGUI.h"

namespace FillTree
{
	enum GameDialogResult
	{
		Close,
		OK,
		Quit
	};

	class GameDialog : public Node
	{
	public:
		typedef function<void(GameDialogResult)> DIALOG_CALLBACK;

		GameDialog(CREF(UIConfig::GameDialog) config);
		CC_SYNTHESIZE_PASS_BY_REF(DIALOG_CALLBACK, dlgCallback, DialogCallback);

	protected:
		Lock tapLocker;
		ui::Button *dialogButton;
		ui::Button *closeButton;

		function<void(Ref*, ui::Widget::TouchEventType)>
			makeCallback(GameDialogResult);
		virtual void onCallback(GameDialogResult);
	};
}