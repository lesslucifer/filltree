#pragma once
#include "EndGameDialog.h"
#include "GameRating.h"

namespace FillTree
{
	class WinGameDialog : public EndGameDialog
	{
	public:
		WinGameDialog(MatchData *data,
			CREF(UIConfig::WinGameDialog) config,
			CREF(GameRating) rating);

		void startAnimate();
	private:
		void update(float dt) override;
		void updateForeground();
		void updateStars();

		void animateActiveStar(uint star);

		void animationCompleted();

		Node *progresForeground;
		Sprite *progressForegroundImg;
		Rect progressFgFrame;

		vector<float> uiStarRates;
		vector<Node *> uiStarNodes;
		uint currentStar;

		// animations
		float cProgress;
		float destProgress;
		const float maxProgressRate;
		bool animating;

		CREF(UIConfig::WinGameDialog) config;
	};
}