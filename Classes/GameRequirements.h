#pragma once
#include <map>
#include "Cell.h"

using namespace std;

namespace FillTree
{
    typedef hmap<CellType, uint> GameResources;
    
	struct GameRequirements
	{
	public:
		inline GameRequirements() {}
		inline GameRequirements(uint maxMoves,
			const GameResources &resources)
			: resources(resources), maxMoves(maxMoves) {}

	private:
		GameResources resources;
		uint maxMoves;

	public:
		inline const GameResources& getResources() const
		{
			return this->resources;
		}
		PROPERTY_GET(uint, MaxMoves, maxMoves);

		JSONCPP_DEFINE("resources", resources, "maxMoves", maxMoves);
	};
}

JSONCPP_REGISTER_CUSTOM_CLASS(FillTree::GameRequirements);