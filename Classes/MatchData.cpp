#include "MatchData.h"

 namespace FillTree
 {
	 MatchData::MatchData(CREF(GameRequirements) requires) :
		 _delegate(nullptr), requirements(requires),
		 remainMoves(requires.getMaxMoves())
	 {
		 auto resourceReqs = requires.getResources();
		 for (auto require : resourceReqs)
		 {
			 CellType requireColor = require.first;
			 gameScores[requireColor] = ValueProxy<uint>(0); 
			 gameScores[requireColor].setOnValueChanged(
				 [requireColor, this](CREF(uint) oldPoint, CREF(uint) newPoint)
			 {
				 if (_delegate)
				 {
					 _delegate->pointChanged(requireColor, newPoint);
				 }
			 }, false);
		 }

		 remainMoves.setOnValueChanged(
			 [this](CREF(uint) oldMove, CREF(uint) newMove)
		 {
			 if (_delegate)
			 {
				 _delegate->movesChanged(newMove);

				 if (0 == newMove)
				 {
					 _delegate->gameEnded(this);
				 }
			 }
		 });
	 }

	 void MatchData::increasePoint(const GameResources &score)
	 {
		 for (auto &kv : score)
		 {
			 if (Utils::contains(gameScores, kv.first))
			 {
				 this->gameScores[kv.first] = this->gameScores[kv.first]() + kv.second;
			 }
		 }
	 }

	 void MatchData::decreasePoint(const GameResources &score)
	 {
		 for (auto &kv : score)
		 {
			 if (Utils::contains(gameScores, kv.first))
			 {
				 uint point = this->gameScores[kv.first]();
				 if (point > kv.second)
				 {
					 point -= kv.second;
				 }
				 else
				 {
					 point = 0;
				 }

				 this->gameScores[kv.first] = point;
			 }
		 }
	 }

	 void MatchData::decreaseMove()
	 {
		 if (remainMoves() > 0)
		 {
			 remainMoves = remainMoves() - 1;
		 }
	 }

	 bool MatchData::isWin() const
	 {
		 auto reqResources = requirements.getResources();
		 for (auto req : reqResources)
		 {
			 if (gameScores.at(req.first)() < req.second)
			 {
				 return false;
			 }
		 }

		 return true;
	 }

	 uint MatchData::getRemainMoves() const
	 {
		 return this->remainMoves();
	 }

	 uint MatchData::getScore(CellType color) const
	 {
		 if (Utils::contains(gameScores, color))
		 {
			 return gameScores.at(color)();
		 }

		 return 0;
	 }

	 void MatchData::getCropsies(GameResources &corpes) const
	 {
		 corpes.clear();
		 for (auto c : gameScores)
		 {
			 corpes[c.first] = c.second();
		 }
	 }
 }