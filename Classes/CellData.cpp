#include "CellData.h"

namespace FillTree
{
	CellData CellData::EMPTY;
	STATIC_CONSTRUCTOR_IMPL(CellData)
	{
		CellData::EMPTY.type = CellType::Empty;
	}
}