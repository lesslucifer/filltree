#pragma once
#include "GameDialog.h"
#include "UIMenuDialogConfig.h"

namespace FillTree
{
	class MenuDialog : public GameDialog
	{
	public:
		MenuDialog(CREF(UIConfig::MenuDialog) config);
	};
}