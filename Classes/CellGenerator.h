#pragma once
#include "Cell.h"
#include "CellGeneratorConfig.h"
#include "ccHelp.h"

USING_CC_HELP;
USING_NS_CC;

namespace FillTree
{
	class CellGenerator : public Ref
	{
	public:
		PUREF(RefHolder<Cell> nextCell(CREF(Index2D) idx));
	};

	class CellGeneratorFactory
	{
	public:
		static RefHolder<CellGenerator> create(CREF(CellGeneratorConfig) config);
	};
}