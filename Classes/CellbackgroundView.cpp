#include "CellbackgroundView.h"
#include "ccHelp.h"

USING_CC_HELP;

namespace FillTree
{
	string BACKGROUNDS[] = {"bold_cell_background.png",
		"light_cell_background.png"};
	Color4B MASK_COLORS[] = {
		Color4B(0, 0, 0, 0),
		Color4B(0, 255, 0, 255),
		Color4B(0, 0, 255, 255),
		Color4B(255, 0, 0, 255),
		Color4B(0, 255, 0, 255),
		Color4B(0, 0, 255, 255),
		Color4B(255, 0, 0, 255),
		Color4B(0, 255, 0, 255),
		Color4B(0, 0, 255, 255),
		Color4B(255, 0, 0, 255)
	};

	CellbackgroundView::CellbackgroundView(CREF(Index2D) idx,
		CREF(Size) cellSize)
	{
		this->setContentSize(cellSize);

		auto bg = Utils::warpSpriteBySize(BACKGROUNDS[(idx.row + idx.col) % 2],
			cellSize);
		bg->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		this->addChild(bg, 0.f);

		this->lbl = Label::createWithTTF("", "fonts/CooperBlackStd.otf", 24.f,
			Size::ZERO, TextHAlignment::RIGHT, TextVAlignment::BOTTOM);
		this->lbl->setAnchorPoint(Vec2::ANCHOR_BOTTOM_RIGHT);
		this->lbl->setPosition(cellSize.width, 0.f);
		this->lbl->setVisible(false);
		this->addChild(this->lbl, 1.f);
	}

	void CellbackgroundView::bonusChanged(uint newBonus)
	{
		if (newBonus > 0)
		{
			this->lbl->setString(StringUtils::format("+%d ", newBonus));
			this->lbl->setTextColor(MASK_COLORS[newBonus]);
			this->lbl->setVisible(true);
		}
		else
		{
			this->lbl->setString(" ");
			this->lbl->setVisible(false);
		}
	}
}