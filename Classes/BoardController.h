#pragma once
#include "cocos2d.h"
#include "Board.h"
#include "MatchData.h"

namespace FillTree
{
	class BoardController : public Ref
	{
	public:
		PUREF(void tapCell(CREF(Index2D) cellIndex));
		PUREF(void killBoard());

		static BoardController* create(Board *board, MatchData *matchData);
	};
}