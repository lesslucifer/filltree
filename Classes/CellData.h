#pragma once
#include "ccHelp.h"
#include "CellType.h"

namespace FillTree
{
	class CellData
	{
	public:
		inline CellData() {}
		inline CellData(CellType _type) : type(_type) {}
		CC_SYNTHESIZE_READONLY(CellType, type, Type);

	public:
		JSONCPP_DEFINE("type", type);

	public:
		static CellData EMPTY;
		STATIC_CONSTRUCTOR_DECLARE(CellData);
	};
}

JSONCPP_REGISTER_CUSTOM_CLASS(FillTree::CellData);