#pragma once
#include "cocos2d.h"
#include "MatchData.h"
#include "ccHelp.h"
#include "GameConfig.h"
#include "UIMatchSceneConfig.h"

USING_NS_CC;

namespace FillTree
{
	class ScoreBar;
	class GameProgressionColumn;

	class MatchSceneUINode : public Layer
	{
	public:
		inline static MatchSceneUINode* create(CREF(LevelConfig) lvlConf,
			CREF(UIConfig::MatchScene) conf)
		{
			MatchSceneUINode *node = new MatchSceneUINode(lvlConf, conf);
			return CCH_CREATE(node);
		}

		void setScore(CellType, uint);
		void setMoves(uint newMove);

		void setCloseButtonHanlder(CCH_CALLBACK callback);
	private:
		MatchSceneUINode(CREF(LevelConfig) reqs, CREF(UIConfig::MatchScene));

		ScoreBar *scoreBar;
		CCH_FUNCTION closeButtonHandler;
	};
}