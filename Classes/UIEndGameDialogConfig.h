#pragma once
#include "cocos2d.h"
#include "ccHelp.h"
#include "UIGameDialogConfig.h"

USING_NS_CC;
USING_CC_HELP;

namespace FillTree
{
	namespace UIConfig
	{
		class EndGameDialog : public GameDialog
		{
			SYTHESINE_VAR_GET(string, CropsiesBar);
			SYTHESINE_VAR_GET(Size, CropsiesBarSize);
			SYTHESINE_VAR_GET(Vec2, CropsiesBarPos);

			SYTHESINE_VAR_GET(Vec2, ResultCellBegin);
			SYTHESINE_VAR_GET(uint, NResultCellMax);
			SYTHESINE_VAR_GET(float, ResultCellDistance);

			SYTHESINE_VAR_GET(Size, ResultIconSize);
			SYTHESINE_VAR_GET(Vec2, ResultIconPos);
			
			SYTHESINE_VAR_GET(string, ResultTickerSuccess);
			SYTHESINE_VAR_GET(string, ResultTickerFail);
			SYTHESINE_VAR_GET(Size, ResultTickerSize);
			SYTHESINE_VAR_GET(Vec2, ResultTickerPos);
			
			SYTHESINE_VAR_GET(string, ResultFont);
			SYTHESINE_VAR_GET(float, ResultFontSize);
			SYTHESINE_VAR_GET(float, ResultTextPosY);
			SYTHESINE_VAR_GET(Color4B, ResultTextSuccColor);
			SYTHESINE_VAR_GET(Color4B, ResultTextFailColor);

		protected:
			void androidInit() override;

		public:
			static EndGameDialog AndroidConfig;

			STATIC_CONSTRUCTOR_DECLARE(EndGameDialog);
		};

		class LoseGameDialog : public EndGameDialog
		{
			SYTHESINE_VAR_GET(string, LoseTextImg);
			SYTHESINE_VAR_GET(float, TextPosY);
			SYTHESINE_VAR_GET(string, ButtonText);

		protected:
			void androidInit() override;

		public:
			static LoseGameDialog AndroidConfig;

			STATIC_CONSTRUCTOR_DECLARE(LoseGameDialog);
		};

		class WinGameDialog : public EndGameDialog
		{
			SYTHESINE_VAR_GET(Size, ProgressBarBgSize);
			SYTHESINE_VAR_GET(string, ProgressBarBackground);
			SYTHESINE_VAR_GET(Size, ProgressBarFgSize);
			SYTHESINE_VAR_GET(string, ProgressBarForeground);

			SYTHESINE_VAR_GET(Vec2, ProgressBarPos);

			SYTHESINE_VAR_GET(Size, StarSize);
			SYTHESINE_VAR_GET(vector<Vec2>, StarPositions);
			SYTHESINE_VAR_GET(string, InactiveStar);
			SYTHESINE_VAR_GET(string, ActiveStar);

			SYTHESINE_VAR_GET(string, ButtonText);

		protected:
			void androidInit() override;

		public:
			static WinGameDialog AndroidConfig;

			STATIC_CONSTRUCTOR_DECLARE(WinGameDialog);
		};
	}
}