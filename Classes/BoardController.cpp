#include "BoardController.h"
#include <queue>
#include <list>

USING_CC_HELP;
using namespace std;

namespace FillTree
{
	class BoardControllerImpl : public BoardController
	{
	public:
		BoardControllerImpl(Board *board, MatchData *matchData) :
			board(board), matchData(matchData) {}

		void tapCell(CREF(Index2D) index) override
		{
            if (this->activeCell(0, index))
            {
                matchData->decreaseMove();
                if (matchData->getRemainMoves() > 0 && !board->getExtractor().checkBoard())
                {
                    board->shuffle();
                }
            }
        }

		bool activeCell(uint chain, CREF(Index2D) index)
		{
			if (!board->isContains(index) || !board->get(index)->isCollectable())
			{
				return false;
			}
            
            list<Index2D> extractedIdxs;
            board->getExtractor().floodFill(index, extractedIdxs);
            
            if (extractedIdxs.size() < 3)
            {
                return false;
            }
            
            auto score = board->calcScore(extractedIdxs);
            
            board->clearAllBonus(extractedIdxs);
            board->activeCells(extractedIdxs);
            board->giveBonus(extractedIdxs);
            board->arrange();
            
            matchData->increasePoint(score);
            
            this->activeCell(chain + 1, index);
            return true;
        }

		void killBoard() override
		{
            board->killBoard();
		}

	private:
		Board *board;
		MatchData *matchData;
		
		OperationQueue opQueue;
	};

	BoardController* BoardController::create(Board *board, MatchData *matchData)
	{
		return Utils::createnx(new BoardControllerImpl(board, matchData));
	}
}