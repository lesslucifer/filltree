#pragma once
#include "BoardController.h"
#include "CellView.h"

namespace FillTree
{
	class BoardView : public cocos2d::Node, public BoardTransformListener
	{
	public:
		BoardView(cocos2d::Size cellSize);

		void boardDidLoad(Board&) override;
		void cellsDidMove(CREF(Board) board,
			CREF(BoardMoveList) moves) override;
		void boardDidShuffle() override;
		void cellsDidActive(CREF(Board) board, CREF(list<Index2D>) removeds) override;
		void boardDidKilled() override;

	private:
		cocos2d::Vec2 getCellPos(uint row, uint col) const;
		cocos2d::Vec2 getCellPos(CREF(Index2D) idx) const;
		ccHelp::Index2D getCellIndex(CREF(cocos2d::Vec2) pos) const;
		FiniteTimeAction* actionFromMove(const CellView *cell,
			CREF(BoardMove) move) const;

		void putCell(CellView *cell, CREF(Index2D) idx);
		void onTouchEnded(cocos2d::Touch* touches, cocos2d::Event *unused_event);

		BoardController *boardController;
		RefHolder<Array2D<CellView>> cells;
		cocos2d::Size cellSize;
        
	public:
		PROPERTY_GET_SET(BoardController *, BoardController, boardController);
        CCH_SYNTHESIZE_MUTABLE_READONLY_PASS_BY_REF(OperationManager, mOP, OP);
	};
}