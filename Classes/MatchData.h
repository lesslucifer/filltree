#pragma once
#include "cocos2d.h"
#include "ccHelp.h"
#include <vector>
#include <map>
#include "Cell.h"
#include "GameRequirements.h"
#include "Utils.h"

USING_NS_CC;
USING_CC_HELP;
using namespace std;

namespace FillTree
{
	class MatchDataDelegate;

	class MatchData : public Ref
	{
	public:
		MatchData(CREF(GameRequirements) requires);

		void increasePoint(const GameResources &score);
		void decreasePoint(const GameResources &score);
		void decreaseMove();

		bool isWin() const;
		uint getRemainMoves() const;
	private:
		MatchDataDelegate *_delegate;

		GameRequirements requirements;

		hmap<CellType, ValueProxy<uint>> gameScores;
		ValueProxy<uint> remainMoves;
	public:
		PROPERTY_SET(MatchDataDelegate *, Delegate, _delegate);
		PROPERTY_GET(CREF(GameRequirements), Requirements, requirements);

		uint getScore(CellType color) const;
		void getCropsies(GameResources&) const;
	};

	class MatchDataDelegate
	{
	public:
		PUREF(void pointChanged(CellType color, uint newPoint));
		PUREF(void movesChanged(uint newMove));
		PUREF(void gameEnded(const MatchData *data));
	};
}