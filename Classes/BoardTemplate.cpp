#include "BoardTemplate.h"

namespace FillTree
{
	CellData BoardTemplate::operator[](CREF(Index2D) idx) const
	{
		auto it = cells.find(idx);
		if (it != cells.cend())
		{
			return it->second;
		}

		return CellData::EMPTY;
	}
}