#include "MenuDialog.h"

namespace FillTree
{
	MenuDialog::MenuDialog(CREF(UIConfig::MenuDialog) config) :
		GameDialog(config)
	{
		this->dialogButton->setVisible(false);

		auto text = Sprite::create(config.getTextImg());
		text->setPosition(config.getTextImgPos());
		this->addChild(text);

		auto img = Utils::warpSpriteBySize(config.getDlgImg(),
			config.getDlgImgSize());
		img->setPosition(config.getDlgImgPos());
		this->addChild(img);

		
		auto btgButton = ui::Button::create(config.getBackToGameButton());
		btgButton->setSize(config.getBackToGameButtonSize());
		btgButton->setPosition(config.getBackToGameButtonPos());
		btgButton->addTouchEventListener(this->makeCallback(GameDialogResult::OK));
		btgButton->setTitleFontName(config.getDlgButtonFont());
		btgButton->setTitleFontSize(config.getDlgButtonFontSize());
		btgButton->setTitleText("Back To Game");
		this->addChild(btgButton);

		auto quitButton = ui::Button::create(config.getQuitButton());
		quitButton->setSize(config.getQuitButtonSize());
		quitButton->setPosition(config.getQuitButtonPos());
		quitButton->setTitleFontName(config.getDlgButtonFont());
		quitButton->setTitleFontSize(config.getDlgButtonFontSize());
		quitButton->setTitleText("Quit");
		quitButton->addTouchEventListener(this->makeCallback(GameDialogResult::Quit));
		this->addChild(quitButton);
	}
}