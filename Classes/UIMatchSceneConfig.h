#pragma once
#include "cocos2d.h"
#include "Def.h"
#include "UIHeaderBarConfig.h"
#include <string>

using std::string;
USING_NS_CC;

namespace FillTree
{
	namespace UIConfig
	{
		class MatchScene
		{
			SYTHESINE_VAR_GET(string, CloseButton);
			SYTHESINE_VAR_GET(Size, CloseButtonSize);
			SYTHESINE_VAR_GET(Vec2, CloseButtonPos);

			SYTHESINE_VAR_GET(string, CloseButtonBg);
			SYTHESINE_VAR_GET(Size, CloseButtonBgSize);
			SYTHESINE_VAR_GET(Vec2, CloseButtonBgPos);

			inline CREF(UIConfig::HeaderBar) getHeaderBar() const
			{
				return UIConfig::HeaderBar::AndroidUIConfig;
			}
		private:
			void androidInit();

		public:
			static MatchScene Android;

			STATIC_CONSTRUCTOR_DECLARE(MatchScene);
		};
	}
}