#include "UISelectLevelSceneConfig.h"

namespace FillTree
{
	namespace UIConfig
	{
		SelectLevel SelectLevel::AndroidConfig;
		STATIC_CONSTRUCTOR_IMPL(SelectLevel)
		{
			SelectLevel::AndroidConfig.androidInit();
		}

		void SelectLevel::androidInit()
		{
			// init tile
			this->TileConfig.TileSize = Size(113, 116);
			this->TileConfig.PerfectBackground = "perf_tile_bg.png";
			this->TileConfig.NormalBackground = "not_perf_tile_bg.png";
			this->TileConfig.LockedBackground = "locked_tile_bg.png";

			this->TileConfig.SuccesStar = "succ_tile_star.png";
			this->TileConfig.FailedStar = "failed_tile_star.png";

			// init star
			this->TileConfig.Stars.resize(3);

			this->TileConfig.Stars[0].Position = Vec2(20, 45);
			this->TileConfig.Stars[0].Rotation = 45.f;
			this->TileConfig.Stars[1].Position = Vec2(56, 20);
			this->TileConfig.Stars[1].Rotation = 0;
			this->TileConfig.Stars[2].Position = Vec2(90, 45);
			this->TileConfig.Stars[2].Rotation = -45.f;
			this->TileConfig.StarSize = Size(29, 26);

			this->TileConfig.Font = "fonts/SHOWG.TTF";
			this->TileConfig.FontSize = 60;
			this->TileConfig.FontPosition = Vec2(56, 60);

			this->BeginPos = Vec2(78, 1080);
			this->Dist = Vec2(150, -150);
			this->TilePerRow = 4;
		}
	}
}