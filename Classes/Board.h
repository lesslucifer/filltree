#pragma once
#include "ccHelp.h"
#include "Cell.h"
#include "CellGenerator.h"
#include "BoardTemplate.h"
#include "CellbackgroundView.h"
#include "GameRequirements.h"

USING_CC_HELP;
using namespace std;

namespace FillTree
{
	class Board;
	class BoardExtractor;
	class BoardMoveCollector;
	typedef list<Index2D> BoardMove;
	typedef list<BoardMove> BoardMoveList;

	class BoardTransformListener;

	class BoardExtractor
	{
	public:
		BoardExtractor(CREF(Board) board);
		void floodFill(CREF(Index2D) idx,
			_out_ list<Index2D>& cells) const;

		bool checkBoard() const;
	protected:
	private:
		void floodFill(CREF(Index2D) idx,
			const std::function<bool(CREF(Cell), CREF(Index2D))>& filter,
			_out_ list<Index2D>& cells,
			_inout_ bool** visited) const;

		CREF(Board) board;
	};

	class Board : public Array2D<Cell>
	{
	public:
		Board(CREF(BoardTemplate) config,
			RefHolder<CellGenerator> randomer,
			BoardTransformListener *_delegate);

		Cellbackground& getCellbackground(CREF(Index2D) idx);
		const Cellbackground& getCellbackground(CREF(Index2D) idx) const;

		void arrange();
		void shuffle();

		void clearAllBonus(CREF(list<Index2D>) excepts);
		void giveBonus(CREF(list<Index2D>) idxs);
		GameResources calcScore(CREF(list<Index2D>) idxs) const;

		void activeCells(CREF(list<Index2D>) indices);
		void killBoard();

		const BoardExtractor& getExtractor() const;
		BoardExtractor& getExtractor();
	private:
		Matrix<Cellbackground> backgrounds;

		void arrange(BoardMoveCollector &collector);
		bool findPath(CREF(Index2D) target, BoardMove &tracer) const;
		void loadBoard(CREF(BoardTemplate) config);

		BoardTransformListener *_delegate;
		RefHolder<CellGenerator> generator;
		BoardExtractor extractor;
	};

	class BoardTransformListener
	{
	public:
		PUREF(void boardDidLoad(Board&));

		PUREF(void cellsDidMove(CREF(Board), CREF(BoardMoveList)));
		PUREF(void boardDidShuffle());

		PUREF(void cellsDidActive(CREF(Board) board,
			CREF(list<Index2D>) removeds));

		PUREF(void boardDidKilled());
	};
}