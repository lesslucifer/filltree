#pragma once
#include <string>

using std::string;

namespace FillTree
{
	class CellGeneratorConfig
	{
	public:
		SYTHESINE_VAR_GET(string, Type);
		SYTHESINE_VAR_GET(Json::Value, Data);

		JSONCPP_DEFINE("type", Type, "data", Data);
	};
}

JSONCPP_REGISTER_CUSTOM_CLASS(FillTree::CellGeneratorConfig);