#include "AppDelegate.h"
#include "MatchScene.h"
#include "HelloWorldScene.h"
#include "Mainmenu.h"
#include "Global.h"
#include "GameConfig.h"
#include "Random.h"
#include "Operation/OperationManager.h"

USING_NS_CC;
using namespace FillTree;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
	//set OpenGL context attributions,now can only set six attributions:
	//red,green,blue,alpha,depth,stencil
	GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };

	GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
	return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {// initialize director
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	if (!glview) {
		glview = GLViewImpl::create("My Game");
		director->setOpenGLView(glview);
	}

	// turn on display FPS
	director->setDisplayStats(true);

	// set FPS. the default value is 1.0/60 if you don't call this
	director->setAnimationInterval(1.0 / 60);

	register_all_packages();

	// handle resolutions
	Size resResolution(720, 1280);
	Size degResolution(720, 1280);

#ifdef CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
	glview->setFrameSize(360, 640);
#endif

	glview->setDesignResolutionSize(degResolution.width, degResolution.height,
		ResolutionPolicy::SHOW_ALL);
	director->setContentScaleFactor(resResolution.height / resResolution.height);

	// load custom fonts:
	vector<string> fonts;
	fonts.push_back("fonts/BlackoakStd.otf");
	fonts.push_back("fonts/CooperBlackStd.otf");
	fonts.push_back("fonts/Marker Felt.ttf");
	fonts.push_back("fonts/SHOWG.ttf");
	Utils::loadCustomFonts(fonts);

    // create a scene. it's an autorelease object
	Global::loadGameData();
	Json::Value gameConfig;
	Utils::readResourceJSON("Game.data", gameConfig);
	GameConfig::CONF.deserialize(gameConfig);
	Scene *scene = Utils::createScene<MainmenuScene>();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
