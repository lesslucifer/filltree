#include "UIMatchSceneConfig.h"

namespace FillTree
{
	namespace UIConfig
	{
		MatchScene MatchScene::Android;
		STATIC_CONSTRUCTOR_IMPL(MatchScene)
		{
			MatchScene::Android.androidInit();
		}

		void MatchScene::androidInit()
		{
			this->CloseButtonBg = "game_menu_button_bg.png";
			this->CloseButtonBgSize = Size(109, 117);
			this->CloseButtonBgPos = Vec2(60, 167);

			this->CloseButton = "game_menu_button.png";
			this->CloseButtonSize = Size(58, 58);
			this->CloseButtonPos = this->CloseButtonBgSize / 2;
		}
	}
}