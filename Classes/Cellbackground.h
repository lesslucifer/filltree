#pragma once
#include "ccHelp.h"

USING_NS_CC;
USING_CC_HELP;

namespace FillTree
{
	class CellbackgroundDelegate;

	class Cellbackground
	{
	public:
		ValueProxy<uint> bonus;
		CellbackgroundDelegate *delegate;

		Cellbackground();
		Cellbackground(CREF(Cellbackground) bg);
		CREF(Cellbackground) operator=(CREF(Cellbackground) bg);

	private:
		void init();
	};

	class CellbackgroundDelegate
	{
	public:
		PUREF(void bonusChanged(uint newBonus));
	};
}