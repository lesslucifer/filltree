#pragma once
#include "cocos2d.h"
#include "UISelectLevelSceneConfig.h"
#include "GameConfig.h"
#include "SelectLevels.h"

USING_NS_CC;

namespace FillTree
{
	class SelectLevelTile : public Node
	{
	public:
		SelectLevelTile(CREF(UIConfig::SelectLevel::Tile) conf,
			uint levelIndex, uint star, LevelMode mode, LevelSelectedCallback callback);
	protected:
	private:
		uint index;
		LevelSelectedCallback callback;
	};
}