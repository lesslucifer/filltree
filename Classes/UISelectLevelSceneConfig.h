#pragma once
#include "cocos2d.h"
#include "ccHelp.h"

USING_NS_CC;
USING_CC_HELP;

namespace FillTree
{
	namespace UIConfig
	{
		class SelectLevel
		{
		public:
			class Tile
			{
			public:
				class Star
				{
				public:
					SYTHESINE_VAR_GET(Vec2, Position);
					SYTHESINE_VAR_GET(float, Rotation);

					friend class SelectLevel;
				};

				SYTHESINE_VAR_GET(Size, TileSize);
				SYTHESINE_VAR_GET(string, PerfectBackground);
				SYTHESINE_VAR_GET(string, NormalBackground);
				SYTHESINE_VAR_GET(string, LockedBackground);

				SYTHESINE_VAR_GET(string, SuccesStar);
				SYTHESINE_VAR_GET(string, FailedStar);

				SYTHESINE_VAR_GET(vector<Star>, Stars);
				SYTHESINE_VAR_GET(Size, StarSize);

				SYTHESINE_VAR_GET(string, Font);
				SYTHESINE_VAR_GET(float, FontSize);
				SYTHESINE_VAR_GET(Vec2, FontPosition);

				friend class SelectLevel;
			};

			SYTHESINE_VAR_GET(Tile, TileConfig);
			SYTHESINE_VAR_GET(Vec2, BeginPos);
			SYTHESINE_VAR_GET(Vec2, Dist);
			SYTHESINE_VAR_GET(int, TilePerRow);

		private:
			void androidInit();

		public:
			static SelectLevel AndroidConfig;

			STATIC_CONSTRUCTOR_DECLARE(SelectLevel);
		};
	}
}