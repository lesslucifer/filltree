#pragma once
#include "UIGameDialogConfig.h"

namespace FillTree
{
	namespace UIConfig
	{
		class MenuDialog : public GameDialog
		{
			SYTHESINE_VAR_GET(string, TextImg);
			SYTHESINE_VAR_GET(Vec2, TextImgPos);

			SYTHESINE_VAR_GET(Size, DlgImgSize);
			SYTHESINE_VAR_GET(Vec2, DlgImgPos);
			SYTHESINE_VAR_GET(string, DlgImg);

			SYTHESINE_VAR_GET(Size, BackToGameButtonSize);
			SYTHESINE_VAR_GET(Vec2, BackToGameButtonPos);
			SYTHESINE_VAR_GET(string, BackToGameButton);

			SYTHESINE_VAR_GET(Size, QuitButtonSize);
			SYTHESINE_VAR_GET(Vec2, QuitButtonPos);
			SYTHESINE_VAR_GET(string, QuitButton);

		protected:
			void androidInit() override;

		public:
			static MenuDialog AndroidConfig;

			STATIC_CONSTRUCTOR_DECLARE(MenuDialog);
		};
	}
}