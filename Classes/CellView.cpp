#include "CellView.h"
#include <string>

USING_NS_CC;
USING_CC_HELP;
using namespace std;

namespace FillTree
{
	void CellView::cellDidActive(Cell *cell, CREF(Cellbackground) bg, CCH_CALLBACK _compl)
	{
		_compl();
	}

	CropseyCell::CropseyCell(CREF(Cell) cell, cocos2d::Size contentSize)
		: CellView(contentSize)
	{
		string cellSpriteName = cellSpriteByColor(cell.getData().getType());

		if (!cellSpriteName.empty())
		{
			auto image = Utils::warpSpriteBySize(cellSpriteName, contentSize);
			image->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
			this->addChild(image);
		}
		this->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	}

	void CropseyCell::cellDidActive(Cell *cell, CREF(Cellbackground) bg, CCH_CALLBACK _compl)
	{
		_compl();
	}
}