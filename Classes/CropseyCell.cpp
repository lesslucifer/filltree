#include "StdCell.h"

namespace FillTree
{
	namespace Cells
	{
		class CropseyCell : public StdCell
		{
		public:
			CropseyCell(CREF(CellData) data) :
				StdCell(data, false, true)
			{
				CCASSERT(isCropsey(data.getType()), "Cropsey cell must be cropsey type");
			}
		};

		Cell* getCropsey(CREF(CellData) data)
		{
			return Utils::createnx(new CropseyCell(data));
		}
	}
}