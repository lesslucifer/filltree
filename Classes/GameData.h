#pragma once
#include "ccHelp.h"
#include <vector>
#include "GameConfig.h"

using namespace std;

namespace FillTree
{
	class Global;
	class GameData;

	class GameLevelData
	{
	private:
		uint point;
		uint star;

		friend GameData;
	public:
		inline GameLevelData() : point(0), star(0) {}
		inline GameLevelData(uint p, uint s)
		{
			this->point = p;
			this->star = s;
		}

		PROPERTY_GET_SET(uint, Point, point);
		PROPERTY_GET_SET(uint, Star, star);
		MSGPACK_DEFINE(point, star);
	};

	class GameData
	{
	private:
		inline GameData() {}
		vector<GameLevelData> levels;

		friend Global;
	public:
		void completeLevel(uint lvl, uint point, uint star);
		uint getStar(uint level);
		uint getPoint(uint level);
		LevelMode getMode(uint level);

		PROPERTY_GET(vector<GameLevelData>, Levels, levels);
		PROPERTY_GET(uint, ReachedLevel, levels.size());
		MSGPACK_DEFINE(levels);
	};
}