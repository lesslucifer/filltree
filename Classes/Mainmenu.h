#pragma once
#include "cocos2d.h"

namespace FillTree
{
	class MainmenuScene : public cocos2d::Layer
	{
	public:
		virtual bool init() override;
		CREATE_FUNC(MainmenuScene);
	};
}