#include "LoseGameDialog.h"

namespace FillTree
{
	LoseGameDialog::LoseGameDialog(MatchData *data, CREF(UIConfig::LoseGameDialog) config)
		: EndGameDialog(data, config)
	{
		auto textImg = Sprite::create(config.getLoseTextImg());
		textImg->setPosition(config.getDialogSize().width / 2, config.getTextPosY());
		this->addChild(textImg);

		this->dialogButton->setTitleText(config.getButtonText());
	}
}