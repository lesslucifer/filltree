#include "GameProgressionColumn.h"

USING_CC_HELP;

namespace FillTree
{
	GameProgressionColumn::GameProgressionColumn(
		CREF(UIConfig::HeaderBar::GameProgressionColumn) _config,
		CREF(GameRating) _rator) : rator(_rator), config(_config),
		targetRate(0.f), currentRate(0.f), maxChangeRate(0.01f), currentStar(0)
	{
		auto bg = Utils::warpSpriteBySize(config.getBackground(),
			config.getColumnBgSize());
		bg->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		this->addChild(bg);

		auto inside = Utils::warpSpriteBySize(config.getProgressBg(),
			config.getColumnProgressSize(), 1);
		inside->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		inside->setPosition(config.getProgressPos());
		insideImg = static_cast<Sprite *>(inside->getChildByTag(1));
		insideOrgFrame = insideImg->getTextureRect();
		this->addChild(inside);
		editProgressFrame(currentRate);

		uiStarRates[0] = 0.f;
		for (uint i = 0; i < config.getStarPositions().size(); ++i)
		{
			auto star = Utils::warpSpriteBySize(config.getInactiveStar(),
				config.getStarSize());
			star->setPosition(config.getStarPositions()[i]);
			this->addChild(star);
			this->starNodes.push_back(star);
			uiStarRates[_rator.getStars()[i]] = (star->getPosition().y /
				config.getColumnProgressSize().height);
		}

		auto max1 = *_rator.getStars().crbegin();
		auto max2 = *(_rator.getStars().crbegin() + 1);
		uiStarRates[2*max1 - max2] = 1.f;

		this->scheduleUpdate();
	}


	void GameProgressionColumn::pointChanged(CellType color, uint newPoint)
	{
		corpes[color] = newPoint;

		uint point = rator.rate(corpes);

		auto highPoint = uiStarRates.upper_bound(point);
		if (highPoint != uiStarRates.cend())
		{
			auto lowPoint = highPoint; --lowPoint;
			float uiRate = highPoint->second - lowPoint->second;
			uiRate *= float(highPoint->first) - float(point);
			uiRate /= float(highPoint->first) - float(lowPoint->first);
			uiRate = highPoint->second - uiRate;

			targetRate = uiRate;
		}
		else
		{
			targetRate = 1.f;
		}

		uint star = rator.rateStar(point);
		while (currentStar < star)
		{
			animateShowStar(currentStar);
			++currentStar;
		}
	}

	void GameProgressionColumn::update(float dt)
	{
		if (currentRate != targetRate)
		{
			float d = targetRate - currentRate;
			if (fabs(d) > fabs(maxChangeRate))
			{
				d = (d > 0)?maxChangeRate:-maxChangeRate;
				currentRate += d;
			}
			else
			{
				currentRate = targetRate;
			}

			editProgressFrame(currentRate);
		}
	}

	void GameProgressionColumn::editProgressFrame(float rate)
	{
		rate = 1.f - max(min(rate, 1.f), 0.f);
		Rect frame = insideOrgFrame;
		float d = rate * frame.size.height;
		frame.origin.y += d;
		frame.size.height -= d;
		insideImg->setTextureRect(frame);
	}

	void GameProgressionColumn::animateShowStar(uint st)
    {
        Node *newStar = Sprite::create(config.getActiveStar());
        newStar->setPosition(config.getStarPositions()[st]);
        this->addChild(newStar);
        
        const float animDur = 0.3f;
        const float begScale = 5.0f;
        const float endScale = 1.0f;
        const float rotate = 50.f;
        
        auto initAnim = CallFunc::create([=]
                                         {
                                             newStar->setScale(begScale);
                                             newStar->setRotation(rotate);
                                         });
        
        auto scaleAnim = ScaleTo::create(animDur, endScale);
        auto rotateAnim = RotateBy::create(animDur, -rotate);
        auto mainAnim = Spawn::createWithTwoActions(scaleAnim, rotateAnim);
        
        auto anim = Sequence::create(initAnim, mainAnim,
                                     CallFunc::create([=]
                                                      {
                                                          auto oldStar = this->starNodes[st];
                                                          if (oldStar) {oldStar->removeFromParent();}
                                                          this->starNodes[st] = newStar;
                                                      }), nullptr);
        showStarOPs.add(mkop(newStar, anim));
	}
}