#pragma once
#include "ccHelp.h"
#include "cocos2d.h"
#include "CellType.h"
#include "CellActiveMode.h"
#include "CellData.h"

USING_CC_HELP;

namespace FillTree
{
	class CellDelegate;
	class Cellbackground;

	class Cell : public cocos2d::Ref
	{
	public:
		PUREF(bool isStatic() const);
		PUREF(bool isCollectable() const);
		PUREF(CREF(CellData) getData() const);

		PUREF(CellDelegate* getDelegate());
		PUREF(CellDelegate* setDelegate(CellDelegate *del));

		PUREF(CellActiveMode active(CREF(Cellbackground), CCH_CALLBACK callback));
	};

	class CellDelegate
	{
	public:
		PUREF(void cellDidActive(Cell *cell,
			CREF(Cellbackground) cellBg, CCH_CALLBACK completion));
	};
}