#include "UIEndGameDialogConfig.h"

namespace FillTree
{
	namespace UIConfig
	{
		EndGameDialog EndGameDialog::AndroidConfig;
		STATIC_CONSTRUCTOR_IMPL(EndGameDialog)
		{
			EndGameDialog::AndroidConfig.androidInit();
		}

		void EndGameDialog::androidInit()
		{
			GameDialog::androidInit();

			CropsiesBar = "end_game_dialog_cropsies_bg.png";
			CropsiesBarSize = Size(614, 115);
			this->CropsiesBarPos = (getDialogSize() - CropsiesBarSize) / 2;
			CropsiesBarPos.y -= 20.f;

			this->ResultCellBegin = Vec2(50, 163);
			this->NResultCellMax = 4;
			this->ResultCellDistance = 145;

			this->ResultIconSize = Size(50, 50);
			this->ResultIconPos = Size(77, 56);

			this->ResultTickerSuccess = "end_game_dlg_checkbox_success.png";
			this->ResultTickerFail = "end_game_dlg_checkbox_fail.png";
			this->ResultTickerSize = Size(30, 30);
			this->ResultTickerPos = Vec2(100, 40);

			this->ResultFont = "fonts/CooperBlackStd.otf";
			this->ResultFontSize = 24;
			this->ResultTextPosY = 5;
			this->ResultTextSuccColor = Color4B(0, 255, 0, 255);
			this->ResultTextFailColor = Color4B(106, 2, 2, 255);
		}

		LoseGameDialog LoseGameDialog::AndroidConfig;
		STATIC_CONSTRUCTOR_IMPL(LoseGameDialog)
		{
			LoseGameDialog::AndroidConfig.androidInit();
		}

		void LoseGameDialog::androidInit()
		{
			EndGameDialog::androidInit();

			this->LoseTextImg = "you_lose_text.png";
			this->TextPosY = 331;
			this->ButtonText = "Replay";
		}

		WinGameDialog WinGameDialog::AndroidConfig;
		STATIC_CONSTRUCTOR_IMPL(WinGameDialog)
		{
			WinGameDialog::AndroidConfig.androidInit();
		}

		void WinGameDialog::androidInit()
		{
			EndGameDialog::androidInit();

			this->ProgressBarBgSize = Size(531, 58);
			this->ProgressBarBackground = "end_game_dialog_progress_bg.png";
			this->ProgressBarFgSize = Size(525, 54);
			this->ProgressBarForeground = "end_game_dialog_progress_highlight.png";

			this->ProgressBarPos =
				(this->getDialogSize() - this->ProgressBarBgSize) / 2;
			this->ProgressBarPos.y += 115;

			this->StarSize = Size(113, 113);

			this->StarPositions.push_back(Vec2(112, ProgressBarFgSize.height / 2));
			this->StarPositions.push_back(Vec2(262, ProgressBarFgSize.height / 2));
			this->StarPositions.push_back(Vec2(412, ProgressBarFgSize.height / 2));

			this->InactiveStar = "end_game_dialog_progress_star_normal.png";
			this->ActiveStar = "end_game_dialog_progress_star_active.png";
			
			this->ButtonText = "Next";
		}
	}
}