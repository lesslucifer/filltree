#include "CellGenerator.h"
#include "Cells.h"

namespace FillTree
{
	class DefaultRandomer : public CellGenerator
	{
	public:
		DefaultRandomer()
		{
			srand(time(nullptr));
		}

		RefHolder<Cell> nextCell(CREF(Index2D) idx) override
		{
			CellType type = Cells::CropseyTypes[rand() % Cells::CropseyTypes.size()];
			return Cells::getCropsey(type);
		}
	};

	class ByFreqGenerator : public CellGenerator
	{
	public:
		ByFreqGenerator(Json::Value dataConf)
		{
			srand(time(nullptr));
			map<CellType, uint> freq;
			Json::type::deserialize(dataConf, freq);

			totalFreq = 0;
			for (const auto &c : freq)
			{
				data[totalFreq] = c.first;
				totalFreq += c.second;
			}
		}

		RefHolder<Cell> nextCell(CREF(Index2D) idx) override
		{
			uint gen = rand() % totalFreq;
			auto geni = data.upper_bound(gen); --geni;
			return Cells::getCropsey(geni->second);
		}
	private:
		map<uint, CellType> data;
		uint totalFreq;
	};

	RefHolder<CellGenerator> CellGeneratorFactory
		::create(CREF(CellGeneratorConfig) config)
	{
		if (config.getType() == "default")
		{
			return Utils::createnx(new DefaultRandomer());
		}
		else if (config.getType() == "byfreq")
		{
			return Utils::createnx(new ByFreqGenerator(config.getData()));
		}

		return nullptr;
	}
}