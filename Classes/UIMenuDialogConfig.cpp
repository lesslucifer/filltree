#include "UIMenuDialogConfig.h"

namespace FillTree
{
	namespace UIConfig
	{
		MenuDialog MenuDialog::AndroidConfig;
		STATIC_CONSTRUCTOR_IMPL(MenuDialog)
		{
			MenuDialog::AndroidConfig.androidInit();
		}

		void MenuDialog::androidInit()
		{
			GameDialog::androidInit();

			this->TextImg = "menu_dialog_text.png";
			this->TextImgPos = Vec2(0.7f * getDialogSize().width, 250.f);

			this->DlgImgSize = Size(225, 320);
			this->DlgImgPos = Vec2(0.3f * getDialogSize().width, 280.f);
			this->DlgImg = "menu_dialog_img.png";

			this->BackToGameButtonSize = Vec2(355, 87);
			this->BackToGameButtonPos = Vec2(0.3f * getDialogSize().width, 65.f);
			this->BackToGameButton = "menu_dialog_button_backtogame.png";

			this->QuitButtonSize = Vec2(231, 87);
			this->QuitButtonPos = Vec2(0.8f * getDialogSize().width,
				BackToGameButtonPos.y);
			this->QuitButton = "menu_dialog_button_quit.png";
		}
	}
}