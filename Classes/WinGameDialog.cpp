#include "WinGameDialog.h"
#include <map>

namespace FillTree
{
	WinGameDialog::WinGameDialog(MatchData *data,
		CREF(UIConfig::WinGameDialog) config,
		CREF(GameRating) rating) :
		EndGameDialog(data, config),
		config(config),
		cProgress(0.f), maxProgressRate(0.01f),
		currentStar(0), animating(false)
	{
		auto progressBg = Utils::warpSpriteBySize(
			config.getProgressBarBackground(), config.getProgressBarBgSize());
		progressBg->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		progressBg->setPosition(config.getProgressBarPos());
		this->addChild(progressBg);
		cocos2d::Node *progressFg = Utils::warpSpriteBySize(
			config.getProgressBarForeground(), config.getProgressBarFgSize(), 1);
		progressFg->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		progressFg->setPosition(progressBg->getContentSize() / 2);
		progressBg->addChild(progressFg);
		this->progresForeground = progressFg;
		this->progressForegroundImg = dynamic_cast<Sprite *>(
			progressFg->getChildByTag(1));
		this->progressFgFrame = this->progressForegroundImg->getTextureRect();

		GameResources cropsies;
		data->getCropsies(cropsies);

		for (uint i = 0; i < config.getStarPositions().size(); ++i)
		{
			float rate = config.getStarPositions()[i].x /
				config.getProgressBarFgSize().width;
			this->uiStarRates.push_back(rate);

			auto inActiveStar = Utils::warpSpriteBySize(
				config.getInactiveStar(), config.getStarSize());
			inActiveStar->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
			inActiveStar->setPosition(config.getStarPositions()[i]);
			progresForeground->addChild(inActiveStar);
			uiStarNodes.push_back(inActiveStar);
		}

		uint point = rating.rate(cropsies);
		uint star = rating.rateStar(point);
		destProgress = (star < uiStarRates.size())?uiStarRates[star - 1]:1.f;

		this->dialogButton->setTitleText(config.getButtonText());

		this->startAnimate();
	}

	void WinGameDialog::startAnimate()
	{
		if (!animating)
		{
			tapLocker.unlock();
			animating = true;
			this->scheduleUpdate();
		}
	}

	void WinGameDialog::update(float dt)
	{
		if (animating && cProgress != destProgress)
		{
			float d = destProgress - cProgress;
			if (fabs(d) > maxProgressRate)
			{
				d = (d > 0)?maxProgressRate:(-maxProgressRate);
				cProgress += d;
			}
			else
			{
				cProgress = destProgress;
				this->animationCompleted();
			}

			this->updateForeground();
			this->updateStars();
		}
	}

	void WinGameDialog::updateForeground()
	{
		float rate = 1.f - max(min(cProgress, 1.f), 0.f);
		Rect frame = this->progressFgFrame;
		float d = rate * frame.size.width;
		frame.size.width -= d;
		this->progressForegroundImg->setTextureRect(frame);
	}

	void WinGameDialog::updateStars()
	{
		if (currentStar >= uiStarRates.size())
		{
			return;
		}

		if (cProgress >= uiStarRates[currentStar])
		{
			this->animateActiveStar(currentStar);
			++currentStar;
		}
	}

	void WinGameDialog::animateActiveStar(uint st)
	{
		Node *newStar = Utils::warpSpriteBySize(config.getActiveStar(),
			config.getStarSize());
		newStar->setPosition(config.getStarPositions()[st]);
		progresForeground->addChild(newStar);

		const float animDur = 0.3f;
		const float begScale = 3.0f;
		const float endScale = 1.0f;
		const float rotate = 50.f;

		auto initAnim = CallFunc::create([=]
		{
			newStar->setScale(begScale);
			newStar->setRotation(rotate);
		});

		auto scaleAnim = ScaleTo::create(animDur, endScale);
		auto rotateAnim = RotateBy::create(animDur, -rotate);
		auto mainAnim = Spawn::createWithTwoActions(scaleAnim, rotateAnim);

		auto anim = Sequence::create(initAnim, mainAnim,
			CallFunc::create([=]
		{
			auto oldStar = this->uiStarNodes[st];
			if (oldStar) {oldStar->removeFromParent();}
			this->uiStarNodes[st] = newStar;
		}), nullptr);
		newStar->runAction(anim);
	}

	void WinGameDialog::animationCompleted()
	{
		animating = false;
		this->unscheduleUpdate();
		tapLocker.unlock();
	}
}