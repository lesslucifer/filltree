#pragma once
#include "MatchSceneUINode.h"
#include "UIHeaderBarConfig.h"

namespace FillTree
{
	class GameProgressionColumn : public Node
	{
	public:
		GameProgressionColumn(CREF(UIConfig::HeaderBar::GameProgressionColumn) _config,
			CREF(GameRating) _rator);

		void pointChanged(CellType color, uint newPoint);
		void update(float dt) override;
	private:
		CREF(GameRating) rator;
		CREF(UIConfig::HeaderBar::GameProgressionColumn) config;
		GameResources corpes;
		map<uint, float> uiStarRates;
		Sprite *insideImg;
		Rect insideOrgFrame;
		vector<Node *> starNodes;

		const float maxChangeRate;
		float targetRate;
		float currentRate;

		uint currentStar;
        OperationManager showStarOPs;

		void editProgressFrame(float rate);
		void animateShowStar(uint st);
	};
}