#pragma once
#include "ccHelp.h"
#include "GameRequirements.h"
#include "GameRating.h"
#include "CellGeneratorConfig.h"
#include "BoardTemplate.h"

USING_CC_HELP;

namespace FillTree
{
	class GameConfig;

	enum class LevelMode
	{
		Passed,
		New,
		Locked
	};

	class LevelConfig
	{
	public:
		inline LevelConfig() {}

		SYTHESINE_VAR_GET(GameRequirements, Reqs);
		SYTHESINE_VAR_GET(GameRating, Rate);
		SYTHESINE_VAR_GET(BoardTemplate, Board);
		SYTHESINE_VAR_GET(Size, CellSize);
		SYTHESINE_VAR_GET(CellGeneratorConfig, CellGenerator);

		JSONCPP_DEFINE("Reqs", Reqs, "rate", Rate, "Generator", CellGenerator,
			"Board", Board, "CellSize", CellSize);
	private:
		friend class GameConfig;
	};

	class GameConfig
	{
    public:
        static GameConfig CONF;
        
	public:
		SYTHESINE_VAR_GET(vector<LevelConfig>, Levels);
		inline uint getNumberOfLevels() {return Levels.size();}
		inline const LevelConfig& getLevel(uint index) {return Levels[index];}

		JSONCPP_DEFINE("Levels", Levels);
	private:
	};
}

JSONCPP_REGISTER_CUSTOM_CLASS(FillTree::LevelConfig);
JSONCPP_REGISTER_CUSTOM_CLASS(FillTree::GameConfig);