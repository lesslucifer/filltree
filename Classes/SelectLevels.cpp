#include "SelectLevels.h"
#include "UISelectLevelSceneConfig.h"
#include "GameConfig.h"
#include "SelectLevelTile.h"
#include "Global.h"
#include "MatchScene.h"

namespace FillTree
{
	bool SelectLevelScene::init()
	{
		if (!Layer::init())
		{
			return false;
		}

		auto background = Sprite::create("SelectLevel_Bg.jpg");
		background->setAnchorPoint( Vec2::ANCHOR_BOTTOM_LEFT);
		this->addChild(background);

		return true;
	}

	void SelectLevelScene::onTileSelected(uint index)
	{
		CREF(LevelConfig) config = GameConfig::CONF.getLevel(index);
		auto playScene = Utils::createScene(Utils::create(new MatchScene(config, index)));
		Director::getInstance()->pushScene(playScene);
	}

	void SelectLevelScene::onEnter()
	{
		CREF(UIConfig::SelectLevel) conf = UIConfig::SelectLevel::AndroidConfig;
		Vec2 pos = conf.getBeginPos();
		int col = 0;
		LevelSelectedCallback callback = 
			[this](uint index) {this->onTileSelected(index);};

		for (auto tile : tiles)
		{
			tile->removeFromParent();
		}
		tiles.clear();

		for (uint i = 0; i < GameConfig::CONF.getNumberOfLevels(); ++i)
		{
			LevelMode mode = Global::getGameData().getMode(i);
			int star = Global::getGameData().getStar(i);
			SelectLevelTile *tile = Utils::createnx(
				new SelectLevelTile(conf.getTileConfig(), i, star, mode, callback));
			tile->setPosition(pos);
			this->addChild(tile);
			tiles.push_back(tile);

			++col;
			if (col >= conf.getTilePerRow())
			{
				col = 0;
				pos.x = conf.getBeginPos().x;
				pos.y += conf.getDist().y;
			}
			else
			{
				pos.x += conf.getDist().x;
			}
		}

		Layer::onEnter();
	}
}