#include "BoardView.h"
#include "CellView.h"
#include "Cells.h"
#include "cocos2d.h"
#include "CellGenerator.h"
#include "ShuffleLayer.h"

USING_NS_CC;
USING_CC_HELP;

namespace FillTree
{
	BoardView::BoardView(Size cellSize) :
		cellSize(cellSize) {}

	Vec2 BoardView::getCellPos(uint row, uint col) const
	{
		return Vec2(col * cellSize.width, row * cellSize.height);
	}

	Vec2 BoardView::getCellPos(CREF(Index2D) idx) const
	{
		return getCellPos(idx.row, idx.col);
	}

	Index2D BoardView::getCellIndex(CREF(Vec2) pos) const
	{
		return Index2D(pos.y / cellSize.height, pos.x / cellSize.width);
	}

	void BoardView::boardDidLoad(Board &board)
	{
		uint nRows = board.nRows();
		uint nCols = board.nCols();

		cells = Factory::newArray2D<CellView>(nRows, nCols);
		for (uint row = 0; row < nRows; ++row)
		{
			for (uint col = 0; col < nCols; ++col)
			{
				Index2D idx(row, col);
				auto cell = board.get(row, col);

				if (Cells::isCropsey(cell->getData().getType()))
				{
					CellView *cellView = Utils::createnx(
						new CropseyCell(*cell, cellSize));
					this->putCell(cellView, idx);
				}

				if (cell->isCollectable())
				{
					auto cellBgView = Utils::createnx(
						new CellbackgroundView(idx, cellSize));
					cellBgView->setPosition(cellSize.width * col, cellSize.height * row);
					auto &cellBg = board.getCellbackground(idx);
					cellBg.delegate = cellBgView;

					this->addChild(cellBgView, -1.f);
				}
			}
		}


		this->setContentSize(Size(nCols * cellSize.width,
			nRows * cellSize.height));

		Shortcut::registerTouchUp(this,
			[this](Touch *t, cocos2d::Event *e)
		{
			this->onTouchEnded(t, e);
		});
	}

	void BoardView::boardDidShuffle()
    {
        mOP.newSequence();
        ShuffleLayer::animateShowShuffle(mOP, this, Random::RandomFloat(0.2f, 0.8f),
                                         UIConfig::ShuffleLayer::AndroidConfig);
        mOP.closeCurrent();
	}

	void BoardView::cellsDidMove(CREF(Board) board, CREF(BoardMoveList) moves)
	{
		hmap<const BoardMove*, CellView*> movedCells;
		for (CREF(BoardMove) move : moves)
		{
			auto &start = move.front();
			if (cells->isContains(start) &&
				CellType::Generator != board[start]->getData().getType())
			{
				movedCells[&move] = cells->get(start);
				cells->put(nullptr, start);
			}
			else
			{
				auto cell = Utils::createnx(new CropseyCell(*board[move.back()], cellSize));
                cell->setVisible(false);
				cell->setPosition(getCellPos(start.row, start.col));
				this->addChild(cell);

				movedCells[&move] = cell;
			}
		}

        mOP.newGroup();
        for (CREF(BoardMove) move : moves)
        {
            auto cell = movedCells[&move];
            cells->put(cell, move.back());
            auto moveAction = actionFromMove(cell, move);
            mOP.add(mkop(cell, moveAction));
        }
        mOP.closeCurrent();
	}

#define CELL_MOVE_VEL 800.f
	FiniteTimeAction* BoardView::actionFromMove(const CellView *cell,
		CREF(BoardMove) move) const
	{
		CCASSERT(move.size() >= 2, "Move must have at least 2 steps");

		Vector<FiniteTimeAction *> actions;

		BoardMove::const_iterator first;
		BoardMove::const_iterator last;
		BoardMove::const_iterator curent;

        actions.pushBack(Show::create());
		actions.pushBack(ScaleTo::create(0.05f, 1.f, 1.2f));

		first = move.cbegin();
		last = first; ++last;
		curent = last; ++curent;

		while (curent != move.cend()) 
		{
			bool isCollinear =
				((first->row - last->row) * (first->col - curent->col)) ==
				((first->row - curent->row) * (first->col - last->col));

			if (!isCollinear)
			{
				actions.pushBack(ShortcutAction::moveToBySpeed(
					getCellPos(*first), CELL_MOVE_VEL, getCellPos(*last)));

				first = last;
			}

			last = curent;
			++curent;
		};

		actions.pushBack(ShortcutAction::moveToBySpeed(
			getCellPos(*first), CELL_MOVE_VEL, getCellPos(*last)));

		actions.pushBack(ScaleTo::create(0.1f, 1.f, 0.8f));
		actions.pushBack(ScaleTo::create(0.1f, 1.f, 1.f));

		return Sequence::create(actions);
	}
#undef CELL_MOVE_VEL

	void BoardView::cellsDidActive(CREF(Board) board,
		CREF(list<Index2D>) cellIndices)
	{
		vector<pair<CellView *, uint>> removedCells;
		removedCells.reserve(cellIndices.size());
		for (CREF(Index2D) idx : cellIndices)
		{
			uint score = 1 + board.getCellbackground(idx).bonus();
			removedCells.push_back(make_pair(cells->get(idx), score)); 
			cells->put(nullptr, idx);
		}

		auto removeAction = Sequence::createWithTwoActions(
			EaseBackIn::create(ScaleTo::create(0.2f, 0.f, 0.f)), 
			CallFuncN::create([](Node *node)
		{
			node->removeFromParent();
		}));


        mOP.newGroup();
        for (auto &cell : removedCells)
		{
			if (cell.first)
			{
                mOP.addmk([=](){
                    auto *scoreLblAction = Sequence::createWithTwoActions(Spawn::createWithTwoActions(
                                                                                                      FadeOut::create(0.6f),
                                                                                                      EaseOut::create(MoveBy::create(0.6f, Vec2(0.f, 80.f)), 1.f)
                                                                                                      ),
                                                                          CallFuncN::create([](Node *lbl){lbl->removeFromParent();}));
                    Label *scoreLbl = Label::createWithTTF(StringUtils::format("+%d", cell.second),
                                                           "fonts/CooperBlackStd.otf", 32.f,
                                                           Size::ZERO,
                                                           TextHAlignment::CENTER, TextVAlignment::CENTER);
                    scoreLbl->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
                    scoreLbl->setPosition(cell.first->getPosition() + cellSize / 2);
                    this->addChild(scoreLbl);
                    scoreLbl->runAction(scoreLblAction);
                });

				Utils::setNodeAnchorWithoutChangePosition(cell.first, Vec2(0.5, 0.5));
                mOP.add(mkop(cell.first, removeAction->clone()));
			}
		}
        mOP.closeCurrent();
	}

	void BoardView::boardDidKilled()
	{
		auto removeAction = Sequence::createWithTwoActions(
			EaseBackIn::create(ScaleTo::create(0.2f, 0.f, 0.f)), 
			CallFuncN::create([](Node *node)
		{
			node->removeFromParent();
		}));

        mOP.newGroup();
		Index2D idx;
		for (idx.row = 0; idx.row < cells->nRows(); ++idx.row)
		{
			for (idx.col = 0; idx.col < cells->nCols(); ++idx.col)
			{
				CellView *cell = cells->get(idx);

				if (cell)
				{
					Utils::setNodeAnchorWithoutChangePosition(cell, Vec2(0.5, 0.5));
                    mOP.add(mkop(cell, removeAction->clone()));
                }
			}
		}
        mOP.closeCurrent();
	}

	void BoardView::putCell(CellView *cell, CREF(Index2D) idx)
	{
		CellView *old = cells->get(idx);
		if (old)
		{
			old->removeFromParent();
		}

		cells->put(cell, idx);
		if (cell)
		{
			cell->setPosition(this->getCellPos(idx.row, idx.col));
			this->addChild(cell);
		}
	}
	
	void BoardView::onTouchEnded(Touch* touch, cocos2d::Event *unused_event)
	{
		if (mOP.isOperating())
		{
			return;
		}

		Vec2 touchPos = this->convertToNodeSpace(touch->getLocation());
		Index2D cellIndex = this->getCellIndex(touchPos);

		boardController->tapCell(cellIndex);
	}
}