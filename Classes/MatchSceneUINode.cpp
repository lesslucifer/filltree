#include "MatchSceneUINode.h"
#include "Cell.h"
#include "ui/CocosGUI.h"
#include "UIHeaderBarConfig.h"
#include "GameConfig.h"
#include "GameProgressionColumn.h"

namespace FillTree
{
	class ScoreBar : public Node
	{
	public:
		inline static ScoreBar* create(CREF(LevelConfig) lvlConf, CREF(UIConfig::HeaderBar) conf)
		{
			ScoreBar *scoreBar = new ScoreBar(lvlConf, conf);
			return CCH_CREATE(scoreBar);
		}

		void setScore(CellType color, uint newPoint)
		{
			if (Utils::contains(reqNodes, color))
			{
				auto reqNode = reqNodes[color];

				// update score
				auto scoreLbl = static_cast<Label *>(reqNode->getChildByTag(2));
				scoreLbl->setString(
					Utils::format("%d/%d", newPoint, requirements[color]));

				// update state
				auto scoreIcon = reqNode->getChildByTag(1);
				auto successTick = scoreIcon->getChildByTag(1);
				if (newPoint >= requirements[color])
				{
					successTick->setVisible(true);
					scoreLbl->setTextColor(Color4B(91, 182, 23, 255));
				}
				else
				{
					successTick->setVisible(false);
					scoreLbl->setTextColor(Color4B::BLACK);
				}
			}

			progressCol->pointChanged(color, newPoint);
		}

		void setMoves(uint newMoves)
		{
			this->movesLabel->setString(Utils::format("%d", newMoves));
		}
	private:
		GameResources requirements;
		map<CellType, Node *> reqNodes;
		Label *movesLabel;
		GameProgressionColumn *progressCol;

		ScoreBar(CREF(LevelConfig) lvlConf, CREF(UIConfig::HeaderBar) config) :
			requirements(lvlConf.getReqs().getResources())
		{
			this->setContentSize(config.getScoreBarSize());
			this->setPosition(config.getScoreBarPos());
			this->setAnchorPoint(config.getScorBarAnchor());
			
			{ // init moves cell
				auto movesCell = Node::create();
				movesCell->setContentSize(config.getMovesCellSize());
				movesCell->setPosition(config.getMovesCellPos());

				auto movesBackground = Utils::warpSpriteBySize(
					config.getMovesBackground(), config.getMovesBackgroundSize());
				movesBackground->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
				movesBackground->setPosition(config.getMovesBackgroundPos());
				movesCell->addChild(movesBackground);

				this->movesLabel = Label::createWithTTF("",
					config.getMovesFont(),
					config.getMovesFontSize(),
					Size::ZERO,
					TextHAlignment::CENTER,
					TextVAlignment::CENTER);
				this->movesLabel->setPosition(config.getMovesTextPos());
				this->movesLabel->setTextColor(Color4B::WHITE);
				movesCell->addChild(movesLabel);
				this->setMoves(lvlConf.getReqs().getMaxMoves());

				this->addChild(movesCell);
			}

			{ // init progress column
				progressCol = Utils::createnx(
					new GameProgressionColumn(config.getProgressCol(),
					lvlConf.getRate()));
				progressCol->setPosition(config.getProgressPosition());
				this->addChild(progressCol);
			}

			{ // init req bar
				auto reqBar = Utils::warpSpriteBySize(
					config.getReqBarBackground(), config.getReqBarBackgroundSize());
				reqBar->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
				reqBar->setPosition(config.getReqBarBackgroundPos());
				this->addChild(reqBar);

				Vec2 reqCellPos = config.getReqCellBeginPos();
				for (auto req : requirements)
				{
					auto reqCell = Utils::warpSpriteBySize(config.getReqCellBackground(),
						config.getReqCellBackgroundSize());
					reqCell->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
					reqCell->setPosition(reqCellPos);

					auto reqIcon = Utils::warpSpriteBySize(
						cellSpriteByColor(req.first), config.getReqIconSize());
					reqIcon->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
					reqIcon->setPosition(config.getReqIconPos());
					reqIcon->setTag(1);

					auto successTick = Utils::warpSpriteBySize(
						config.getSuccessTick(), config.getSuccessTickSize());
					successTick->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
					successTick->setPosition(config.getSuccessTickPos());
					successTick->setTag(1);
					reqIcon->addChild(successTick);

					reqCell->addChild(reqIcon);

					auto reqLabel = Label::createWithTTF("",
						config.getReqFontFile(),
						config.getReqFontSize(),
						Size::ZERO,
						TextHAlignment::CENTER,
						TextVAlignment::CENTER);
					reqLabel->setPosition(config.getReqTextPos());
					reqLabel->setTag(2);
					reqCell->addChild(reqLabel);

					this->reqNodes[req.first] = reqCell;
					this->setScore(req.first, 0);

					reqBar->addChild(reqCell);
					reqCellPos.x += config.getReqCellDistance();
				}
			}
		}
	};

	MatchSceneUINode::MatchSceneUINode(CREF(LevelConfig) lvlConf, CREF(UIConfig::MatchScene) config)
	{
		this->scoreBar = ScoreBar::create(lvlConf, config.getHeaderBar());
		this->addChild(scoreBar);

		auto closeBtnBg = Utils::warpSpriteBySize(config.getCloseButtonBg(),
			config.getCloseButtonBgSize());
		closeBtnBg->setPosition(config.getCloseButtonBgPos());
		this->addChild(closeBtnBg);

		auto closeBtn = ui::Button::create(config.getCloseButton());
		closeBtn->setSize(config.getCloseButtonSize());
		closeBtn->setPosition(config.getCloseButtonPos());
		closeBtnBg->addChild(closeBtn);

		closeBtn->addTouchEventListener([this]
		(Ref*, ui::Widget::TouchEventType e)
		{
			if (e == ui::Widget::TouchEventType::ENDED &&
				closeButtonHandler)
			{
				closeButtonHandler();
			}
		});
	}

	void MatchSceneUINode::setScore(CellType color, uint newPoint)
	{
		this->scoreBar->setScore(color, newPoint);
	}
	
	void MatchSceneUINode::setMoves(uint newMove)
	{
		this->scoreBar->setMoves(newMove);
	}

	void MatchSceneUINode::setCloseButtonHanlder(CCH_CALLBACK callback)
	{
		this->closeButtonHandler = callback;
	}
}