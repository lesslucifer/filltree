#include "MatchScene.h"
#include "BoardView.h"
#include "MatchSceneUINode.h"
#include "CellGenerator.h"
#include "UIEndGameDialogConfig.h"
#include "UIMatchSceneConfig.h"
#include "Mainmenu.h"
#include "EndGameDialog.h"
#include "Global.h"
#include "SelectLevels.h"
#include "LoseGameDialog.h"
#include "WinGameDialog.h"
#include "MenuDialog.h"

namespace FillTree
{
	MatchScene::MatchScene(CREF(LevelConfig) _config, uint lvlIndex) :
		config(_config), levelIndex(lvlIndex) {}

	bool MatchScene::init()
	{
		if (!Layer::init())
		{
			return false;
		}

		// load match requirements
		GameRequirements requirements = config.getReqs();
		this->matchData = Utils::createnx(new MatchData(requirements));
		this->matchData->setDelegate(this);

		// load board

		boardView = Utils::createnx(new BoardView(config.getCellSize()));
		this->board = Utils::createnx(new Board(config.getBoard(),
			CellGeneratorFactory::create(config.getCellGenerator()), boardView));
		this->boardController = BoardController::create(board(), matchData());
		boardView->setBoardController(this->boardController());

		Vec2 origin = Director::getInstance()->getVisibleOrigin();
		Size visible = Director::getInstance()->getVisibleSize();
		boardView->setPosition(origin + visible / 2);
		boardView->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		this->addChild(boardView);

		this->uiNode = MatchSceneUINode::create(config, UIConfig::MatchScene::Android);
		this->uiNode->setCloseButtonHanlder([this]
		{
			this->closeButtonTouched();
		});
		this->addChild(uiNode, 2);

		auto background = Utils::warpSpriteBySize("background.png", Size(768, 1280));
		background->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		this->addChild(background, -1);

		capture("");

		return true;
	}

	void MatchScene::capture(string savedFile)
	{
		auto action = Sequence::createWithTwoActions(DelayTime::create(0.5),
			CallFunc::create([=]
		{
			Size scrSize = Director::getInstance()->getWinSize();
			RenderTexture *tex = RenderTexture::create(scrSize.width, scrSize.height);
			tex->begin();
			Director::getInstance()->getRunningScene()->visit();
			tex->end();

			tex->saveToFile("test.jpg", Image::Format::JPG);
			CCLOG("Saved to %s", FileUtils::getInstance()->getWritablePath().c_str() );
		}));
		this->runAction(action);

	}

	void MatchScene::pointChanged(CellType color, uint newPoint)
	{
		this->uiNode->setScore(color, newPoint);
	}

	void MatchScene::movesChanged(uint newMove)
	{
		this->uiNode->setMoves(newMove);
	}

	void MatchScene::gameEnded(const MatchData *data)
	{
        mOP.await(boardView->getOP());
        mOP.addmk([this] {
			bool isWin = this->matchData->isWin();
			EndGameDialog *endGameDialog = nullptr;
			if (!isWin)
			{
				endGameDialog = new LoseGameDialog(this->matchData(),
					UIConfig::LoseGameDialog::AndroidConfig);
			}
			else
			{
				endGameDialog = new WinGameDialog(this->matchData(),
					UIConfig::WinGameDialog::AndroidConfig,
					config.getRate());
			}
			endGameDialog = Utils::createnx(endGameDialog);

			auto dlgLayer = DialogLayer::showDialog(this, endGameDialog, Color4B(),
				DialogAnimation::ZOOM_FADE, DialogAlign::CENTER);
			endGameDialog->setDialogCallback([=](GameDialogResult result)
			{
                mOP.addmk([=](CCH_CALLBACK _compl){
                    this->boardController->killBoard();
                    mOP.await(boardView->getOP());
                    DialogLayer::closeDialog(dlgLayer, endGameDialog,
                                             DialogAnimation::ZOOM_FADE,
                                             [=](DialogLayer *) {_compl();});
                });
                
                mOP.addmk([=] {
                    if (result == GameDialogResult::Close)
                    {
                        this->saveGameData();
                        backToSelectLevels();
                    }
                    else if (isWin)
                    {
                        this->saveGameData();
                        this->tryToNextScene();
                    }
                    else
                    {
                        this->replayMatch();
                    }
                });
			});
		});
	}

	void MatchScene::backToSelectLevels()
	{
		Director::getInstance()->popScene();
	}

	void MatchScene::tryToNextScene()
	{
		if (levelIndex + 1 < GameConfig::CONF.getNumberOfLevels())
		{
			auto &nextConfig = GameConfig::CONF.getLevel(levelIndex + 1);
			Director::getInstance()->replaceScene(
				Utils::createScene(Utils::create(new MatchScene(nextConfig, levelIndex + 1))));
		}
		else
		{
			backToSelectLevels();
		}
	}

	void MatchScene::replayMatch()
	{
		Director::getInstance()->replaceScene(
			Utils::createScene(
			Utils::create(new MatchScene(this->config, levelIndex))));
	}

	void MatchScene::saveGameData()
	{
		hmap<CellType, uint> cropsies;
		matchData->getCropsies(cropsies);
		uint point = config.getRate().rate(cropsies);
		uint star = config.getRate().rateStar(point);
		Global::getGameData().completeLevel(levelIndex, point, star);
		Global::saveGameData();
	}

	void MatchScene::closeButtonTouched()
	{
        mOP.await(boardView->getOP());
		if (mOP.isOperating())
		{
			return;
		}

        mOP.addmk([=](CCH_CALLBACK completion){
			MenuDialog *menu = Utils::createnx(
				new MenuDialog(UIConfig::MenuDialog::AndroidConfig));

			auto *dlg = DialogLayer::showDialog(this, menu, Color4B(),
				DialogAnimation::ZOOM_FADE, DialogAlign::CENTER);

			menu->setDialogCallback([=](GameDialogResult dlgResult)
			{
				DialogLayer::closeDialog(dlg, menu, DialogAnimation::ZOOM_FADE,
						[=](DialogLayer*)
				{
					// notify to board controller
					completion();

					if (dlgResult == GameDialogResult::Quit)
					{
						backToSelectLevels();
					}
				});
			});
		});
	}
}
