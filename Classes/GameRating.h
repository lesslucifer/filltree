#pragma once
#include <vector>
#include <map>
#include "CellType.h"
#include "GameRequirements.h"
#include "ccHelp.h"

using namespace std;

namespace FillTree
{
	class GameRating
	{
	private:
		hmap<CellType, float> corpesRate;
		vector<uint> star;

	public:
		GameRating() {}
		uint rate(const GameResources &cropsies) const;
		uint rateStar(uint point) const;
		PROPERTY_GET(CREF(vector<uint>), Stars, star);

		JSONCPP_DEFINE("rate", corpesRate, "star", star);
	};
}

JSONCPP_REGISTER_CUSTOM_CLASS(FillTree::GameRating);