#pragma once
#include "ccHelp.h"
#include "UIShuffleLayerConfig.h"

USING_CC_HELP;

namespace FillTree
{
	class ShuffleLayer : public Layer
	{
	private:
		RepeatForever* animator;
		Node *loading;

	public:
		ShuffleLayer(CREF(UIConfig::ShuffleLayer) conf);
		void startAnimate();
		void stopAnimate();

		static void animateShowShuffle(OperationManager &op, Node *parent, float dur,
			CREF(UIConfig::ShuffleLayer) config);
	};
}