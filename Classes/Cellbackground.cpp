#include "Cellbackground.h"

namespace FillTree
{
	Cellbackground::Cellbackground() : 
		delegate(nullptr)
	{
		init();
	}

	Cellbackground::Cellbackground(CREF(Cellbackground) bg)
		: delegate(bg.delegate)
	{
		init();
	}

	CREF(Cellbackground) Cellbackground::operator =(CREF(Cellbackground) bg)
	{
		this->delegate = bg.delegate;
		return *this;
	}

	void Cellbackground::init()
	{
		bonus.setOnValueChanged([this](CREF(uint) old, CREF(uint) _new)
		{
			if (this->delegate)
			{
				this->delegate->bonusChanged(_new);
			}
		}, false);
	}
}