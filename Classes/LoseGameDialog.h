#pragma once
#include "EndGameDialog.h"

namespace FillTree
{
	class LoseGameDialog : public EndGameDialog
	{
	public:
		LoseGameDialog(MatchData *data, CREF(UIConfig::LoseGameDialog) config);
	};
}