#include "StdCell.h"
#include "Cellbackground.h"

namespace FillTree
{
	namespace Cells
	{
		StdCell::StdCell(CREF(CellData) data, bool _static, bool _coll) :
				_static(_static), data(data),
				collectable(_coll), _delegate(nullptr) {}

		StdCell::StdCell() :
				data(CellData::EMPTY), _static(true),
				collectable(false), _delegate(nullptr)
			{}

		CREF(CellData) StdCell::getData() const
		{
			return this->data;
		}

		bool StdCell::isStatic() const
		{
			return _static;
		}

		bool StdCell::isCollectable() const
		{
			return collectable;
		}

		CellDelegate* StdCell::getDelegate()
		{
			return this->_delegate;
		}

		CellDelegate* StdCell::setDelegate(CellDelegate *del)
		{
			CellDelegate *oldDel = this->_delegate;
			this->_delegate = del;

			return oldDel;
		}

		CellActiveMode StdCell::active(CREF(Cellbackground) bg, CCH_CALLBACK _compl)
		{
			if (_delegate) 
			{
				_delegate->cellDidActive(this, bg, _compl);
			}

			return CellActiveMode::Remove;
		}
	}
}