#include "UIGameDialogConfig.h"

namespace FillTree
{
	namespace UIConfig
	{
		GameDialog GameDialog::AndroidConfig;
		STATIC_CONSTRUCTOR_IMPL(GameDialog)
		{
			GameDialog::AndroidConfig.androidInit();
		}

		void GameDialog::androidInit()
		{
			this->DialogSize = Size(678, 447);
			this->Background = "end_game_dialog_bg.png";

			this->DlgButtonBgNormal = "end_game_dlg_button_normal.png";
			this->DlgButtonBgSelected = "end_game_dlg_button_selected.png";
			this->DlgButtonFont = "Cooper Std Black";
			this->DlgButtonFontSize = 32;
			this->DlgButtonPos = Vec2(339, 73);

			this->DlgCloseButtonSize = Size(80, 80);
			this->DlgCloseButtonPos = Vec2(628, 392);
			this->DlgCloseButtonImg = "close_btn.png";
		}
	}
}