#pragma once
#include <string>
#include <ccHelp.h>
#include "EnumHelper.h"
#include "Macro/ENUM.hpp"

USING_CC_HELP;

using std::string;

namespace FillTree
{
	typedef enum
	{
		Empty, Generator, Pad,
		Red, Green, Blue, Orange, Purple, Yellow
	} CellType;

	inline string cellSpriteByColor(CellType color)
	{
		switch (color)
		{
		case CellType::Red:
			return "tomato.png";
		case CellType::Green:
			return "sauliflower.png";
		case CellType::Blue:
			return "mushroom.png";
		case CellType::Orange:
			return "carrot.png";
		case CellType::Purple:
			return "eggplant.png";
		case CellType::Yellow:
			return "pear.png";
		}

		return "";
	}
}

namespace std {
    template<>
    struct hash<FillTree::CellType>
    {
        inline long operator()(const FillTree::CellType &v) const
        {
            return static_cast<long>(v);
        }
    };
}

namespace Json
{
	namespace type
	{
		using FillTree::CellType;
		template<> inline string strSerialize(const CellType &cell)
		{
			switch (cell)
			{
			case CellType::Red:
				return "red";
			case CellType::Green:
				return "green";
			case CellType::Blue:
				return "blue";
			case CellType::Orange:
				return "orange";
			case CellType::Purple:
				return "purple";
			case CellType::Yellow:
				return "yellow";
			case CellType::Generator:
				return "gen";
			case CellType::Pad:
				return "pad";
			}

			return "empty";
		}

		template<> inline bool strDeserialize(CREF(string) s, CellType &c)
		{
			c = CellType::Empty;
			if (s == "red") c = CellType::Red;
			else if (s == "green") c = CellType::Green;
			else if (s == "blue") c = CellType::Blue;
			else if (s == "orange") c = CellType::Orange;
			else if (s == "purple") c = CellType::Purple;
			else if (s == "yellow") c = CellType::Yellow;
			else if (s == "gen") c = CellType::Generator;
			else if (s == "pad") c = CellType::Pad;
			else if (s == "empty") c = CellType::Empty;

			return true;
		}

		__JSONSER_TEMPLATE inline Json::Value serialize(const CellType &cell)
		{
			return Json::Value(strSerialize(cell));
		}

		__JSONSER_TEMPLATE inline bool deserialize(CREF(Json::Value) j, CellType &c)
		{
			strDeserialize(j.asString(), c);
			return true;
		}
	}
}