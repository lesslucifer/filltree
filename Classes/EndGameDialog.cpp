#include "EndGameDialog.h"
#include "ui/CocosGUI.h"
#include "Utils.h"

USING_CC_HELP;
using namespace ui;

namespace FillTree
{
	EndGameDialog::EndGameDialog(MatchData *data, CREF(UIConfig::EndGameDialog) config)
		: GameDialog(config)
	{
		auto cropsiesBar = Utils::warpSpriteBySize(
			config.getCropsiesBar(), config.getCropsiesBarSize());
		cropsiesBar->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		cropsiesBar->setPosition(config.getCropsiesBarPos());
		this->addChild(cropsiesBar);

		Vec2 resultCellPos = config.getResultCellBegin();
		uint nReqs = data->getRequirements().getResources().size();
		resultCellPos.x += config.getResultCellDistance() *
			(config.getNResultCellMax() - nReqs) / 2.f;
		
		auto resReqs = data->getRequirements().getResources();
		for (auto req : resReqs)
		{
			Node *resulCell = Node::create();
			resulCell->setPosition(resultCellPos);

			auto icon = Utils::warpSpriteBySize(cellSpriteByColor(req.first),
				config.getResultIconSize());
			icon->setPosition(config.getResultIconPos());
			resulCell->addChild(icon);

			const uint nResHave = data->getScore(req.first);
			const uint nResReq = data->getRequirements().getResources().at(req.first);
			// ticker
			string tickerFileName = (nResHave >= nResReq)?
				config.getResultTickerSuccess():config.getResultTickerFail();
			auto ticker = Utils::warpSpriteBySize(tickerFileName,
				config.getResultTickerSize());
			ticker->setPosition(config.getResultTickerPos());
			resulCell->addChild(ticker);

			// text
			Vec2 textPos = Vec2(config.getResultCellDistance() / 2, config.getResultTextPosY());
			string scoreText = Utils::format("%d/%d", nResHave, nResReq);
			auto scoreLbl = Label::createWithTTF(scoreText,
				config.getResultFont(),
				config.getResultFontSize(),
				Size::ZERO, TextHAlignment::CENTER, TextVAlignment::CENTER);
			scoreLbl->setTextColor((nResHave >= nResReq)?
				config.getResultTextSuccColor():config.getResultTextFailColor());
			scoreLbl->setPosition(textPos);
			resulCell->addChild(scoreLbl);

			this->addChild(resulCell);
			resultCellPos.x += config.getResultCellDistance();
		}
	}
}