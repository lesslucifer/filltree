#include "ShuffleLayer.h"

namespace FillTree
{
	ShuffleLayer::ShuffleLayer(CREF(UIConfig::ShuffleLayer) conf) :
		animator(nullptr)
	{
		auto text = Utils::warpSpriteBySize(conf.getTextImage(),
			conf.getTextSize());
		text->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		this->addChild(text);

		this->loading = Utils::warpSpriteBySize(conf.getLoadingImage(),
			conf.getLoadingSize());
		loading->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		this->addChild(loading);
	}

	void ShuffleLayer::startAnimate()
	{
		if (animator) return;

		animator = RepeatForever::create(RotateBy::create(0.05f, 15.f));
		this->loading->runAction(animator);
	}

	void ShuffleLayer::stopAnimate()
	{
		if (!animator) return;

		loading->stopAction(animator);
		animator = nullptr;
	}

	void ShuffleLayer::animateShowShuffle(OperationManager &op, Node *parent, float dur,
		CREF(UIConfig::ShuffleLayer) config)
    {
        auto *shuffleLayer = Utils::createnx(new ShuffleLayer(config));
        shuffleLayer->retain();
        shuffleLayer->startAnimate();
        
        op.addmk([=, &op](CCH_CALLBACK _compl) {
            auto callback = [=](DialogLayer *) {_compl();};
            auto *dlg = DialogLayer::showDialog(parent, shuffleLayer, Color4B(), ZOOM_FADE, CENTER, callback);
            op.currentContext().putPointer("Dialog", dlg);
        });
        op.delay(dur);
        op.addmk([=, &op](CCH_CALLBACK _compl) {
            shuffleLayer->stopAnimate();
            
            auto *dlg = op.currentContext().getPointer<DialogLayer>("Dialog");
            DialogLayer::closeDialog(dlg, shuffleLayer, ZOOM_FADE, [=](DialogLayer *){
                shuffleLayer->release();
                _compl();
            });
        });
	}
}