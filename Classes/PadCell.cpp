#pragma once
#include "StdCell.h"

namespace FillTree
{
	namespace Cells
	{
		class PadCell : public StdCell
		{
		public:
			static RefHolder<PadCell> CELL;

			CellDelegate* setDelegate(CellDelegate *del)
			{
				return nullptr;
			}

		private:
			PadCell() :
				StdCell(CellType::Pad, true, false) {}
		};

		RefHolder<PadCell> PadCell::CELL =
			Utils::createnx(new PadCell());
		Cell* pad() {return PadCell::CELL();}
	}
}