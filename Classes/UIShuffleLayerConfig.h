#pragma once
#include "cocos2d.h"
#include "ccHelp.h"

USING_NS_CC;
USING_CC_HELP;

namespace FillTree
{
	namespace UIConfig
	{
		class ShuffleLayer
		{
			SYTHESINE_VAR_GET(Size, TextSize);
			SYTHESINE_VAR_GET(string, TextImage);

			SYTHESINE_VAR_GET(Size, LoadingSize);
			SYTHESINE_VAR_GET(string, LoadingImage);

		private:
			void androidInit();
		public:
			static ShuffleLayer AndroidConfig;

			STATIC_CONSTRUCTOR_DECLARE(ShuffleLayer);
		};
	}
}