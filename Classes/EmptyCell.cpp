#include "StdCell.h"

namespace FillTree
{
	namespace Cells
	{
		class EmptyCell : public StdCell
		{
		public:
			static RefHolder<EmptyCell> CELL;

			CellDelegate* setDelegate(CellDelegate *del)
			{
				return nullptr;
			}

		private:
			EmptyCell() : StdCell(CellData::EMPTY, false, false) {}
		};

		RefHolder<EmptyCell> EmptyCell::CELL = Utils::createnx(new EmptyCell());
		Cell* empty() {return EmptyCell::CELL();}
	}
}