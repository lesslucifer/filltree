#pragma once
#include "cocos2d.h"
#include "Cellbackground.h"

namespace FillTree
{
	class CellbackgroundView : public Layer, public CellbackgroundDelegate
	{
	public:
		CellbackgroundView(CREF(Index2D) idx, CREF(Size) cellSize);
		void bonusChanged(uint newBonus) override;

	private:
		Label *lbl;
	};
}