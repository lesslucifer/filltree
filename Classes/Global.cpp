#include "Global.h"
#include "ccHelp.h"
#include "Global.h"
#include "Cells.h"

USING_CC_HELP;

namespace FillTree
{
	string Global::GameDataFile = "User.data";
	GameData Global::gameData;

	GameData& Global::getGameData()
	{
		return gameData;
	}

	void Global::loadGameData()
	{
		Data data = Utils::readGameData(GameDataFile);

		if (data.getSize() > 0)
		{
			msgpack::unpacked result;
			msgpack::unpack(&result,
				(const char*) data.getBytes(),
				(size_t) data.getSize());
			result.get().convert(&Global::gameData);
		}
		else
		{
			gameData = GameData();
		}
	}

	void Global::saveGameData()
	{
		msgpack::sbuffer buffer(0x1000);
		msgpack::pack(buffer, gameData);

		Utils::writeGameData(GameDataFile, buffer.data(), buffer.size());
	}

	const std::function<bool(CREF(list<Index2D>))> Global::MATCH_RULE = []
	(CREF(list<Index2D>) cells) -> bool
	{
		return cells.size() >= 3;
	};

	std::function<bool(CREF(Cell), CREF(Index2D))>
		Global::getFilterByType(CellType type)
	{
		return [=](CREF(Cell) cell, CREF(Index2D) idx) -> bool
		{
			return cell.getData().getType() == type;
		};
	}
}