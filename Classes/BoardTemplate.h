#pragma once
#include "CellData.h"
#include "ccHelp.h"

USING_CC_HELP;

namespace FillTree
{
	class BoardTemplate
	{
	private:
		hmap<Index2D, CellData> cells;
		Index2D boardSize;
		
	public:
		BoardTemplate() {}

		JSONCPP_DEFINE("cells", cells, "boardsize", boardSize);

		CellData operator[](CREF(Index2D) idx) const;
		PROPERTY_GET(CREF(Index2D), BoardSize, boardSize);
	};
}

JSONCPP_REGISTER_CUSTOM_CLASS(FillTree::BoardTemplate);