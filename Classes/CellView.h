#pragma once
#include "cocos2d.h"
#include "Cell.h"
#include "ccHelp.h"

namespace FillTree
{
	class CellView : public cocos2d::Layer, public CellDelegate
	{
	public:
		inline CellView(Size contentSize)
		{
			this->setContentSize(contentSize);
		}
		void cellDidActive(Cell *cell, CREF(Cellbackground) bg, CCH_CALLBACK _compl) override;
	};

	class EmptyCell : public CellView
	{
		inline EmptyCell(Size contentSize) : CellView(contentSize) {}
	};
	
	class CropseyCell : public CellView
	{
	public:
		CropseyCell(CREF(Cell) cell, cocos2d::Size contentSize);
		void cellDidActive(Cell *cell, CREF(Cellbackground) bg, CCH_CALLBACK _compl) override;
	};
}