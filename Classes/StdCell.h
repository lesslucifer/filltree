#pragma once
#include "Cells.h"

namespace FillTree
{
	namespace Cells
	{
		class StdCell : public Cell
		{
		private:
			bool _static, collectable;
			CellDelegate *_delegate;
			CellData data;

		public:
			StdCell(CREF(CellData) data, bool _static, bool _coll);
			StdCell();

			bool isStatic() const override;
			bool isCollectable() const override;
			CREF(CellData) getData() const override;

			CellDelegate* getDelegate() override;
			CellDelegate* setDelegate(CellDelegate *del) override;

			CellActiveMode active(CREF(Cellbackground), CCH_CALLBACK) override;
		};
	}
}