/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package penguin.jsonConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import xlsconfig.BaseXLSConfigurator;

/**
 *
 * @author Salm
 * @param <E>
 */
public class JsonOutputConfigurator<E> extends BaseXLSConfigurator<E>
{
    private static final Gson jsonParser;
    
    static
    {
        jsonParser = new GsonBuilder().setPrettyPrinting()
                .create();
    }
    
    public JsonOutputConfigurator(Class<E> clazz, String file) {
        super(clazz, file);
    }
    
    public JsonOutputConfigurator(Class<E> clazz) {
        super(clazz);
    }

    @Override
    protected void postConfig(E e) throws Exception
    {
        String fileName = this.file.substring(0, file.lastIndexOf("."));
        Map<String, Object> data = new HashMap(1);
        data.put(fileName,
                (e instanceof CustomOutput)?((CustomOutput) e).output():e);
        
        String outFile = fileName + ".xfj";
        writeOut(getOutPath() + "\\" + outFile, data);
        super.postConfig(e);
    }
    
    public static void writeOut(String outFile, Object out)
            throws IOException
    {
        Writer writer = new BufferedWriter(new FileWriter(outFile));
        jsonParser.toJson(out, writer);
        writer.flush();
    }
}
