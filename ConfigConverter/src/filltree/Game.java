/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filltree;

import java.util.List;
import java.util.Map;
import xlsParser.XLSField;
import xlsParser.XLSMap;
import xlsParser.XLSObject;

/**
 *
 * @author Salm
 */
@XLSObject
public class Game {
    @XLSField("Index")
    public List<Level> Levels;
    
    @XLSObject
    public static class Level
    {
        private Requirements Reqs;
        private Rate rate;
        private Generator Generator;
        private CellSize CellSize;
        public Board Board;

        @XLSObject
        public static class Requirements
        {
            @XLSField("Moves")
            int maxMoves;

            @XLSField("ReqType")
            @XLSMap(mapField = "ReqVal")
            Map<String, Integer> resources;
        }

        @XLSObject
        public static class Rate
        {
            @XLSField("ReqType")
            @XLSMap(mapField = "PointRate")
            Map<String, Double> rate;

            @XLSField("Star")
            List<Integer> star;
        }

        @XLSObject
        public static class Generator
        {
            @XLSField("Gen Type")
            String type;

            @XLSField("Gen Field")
            @XLSMap(mapField = "Gen Value")
            Map<String, Integer> data;
        }

        public static class Board
        {
            public static class BoardSize
            {
                @XLSField("nRows")
                public int row;
                @XLSField("nCols")
                public int col;
            }

            @XLSField
            public BoardSize boardsize;

            @XLSField("Board")
            public transient String boardSheet;
            
            public Object cells;
        }

        @XLSObject
        public static class CellSize
        {
            @XLSField("CellWidth")
            int width;
            @XLSField("CellHeight")
            int height;
        }
    }
}
