/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filltree;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import xlsconfig.XLSConfigurator;

/**
 *
 * @author Salm
 */
public class Main {
    private static final List<XLSConfigurator> configs;
    
    static
    {
        configs = new ArrayList();
        configs.add(new FTConfigurator());
    }
    
    public static void main(String[] args)
    {
        String inPath = "../Design/xls";
        String outPath = "../Resources";
        configs.stream().forEach((config) -> {
            config.setInPath(inPath);
            config.setOutPath(outPath);
            new Thread(() -> {
                try {
                    config.runConfig();
                } catch (Exception ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
        });
    }
}
