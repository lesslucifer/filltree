/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filltree;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import penguin.jsonConfig.CustomOutput;
import xlsParser.XLSObject;
import xlsParser.XLSRecord;

/**
 *
 * @author Salm
 */
@XLSObject
public class LevelBoard implements CustomOutput {
    
    private Map<Index2D, String> cells;
    private final int nRows, nCols;
    
    public LevelBoard(int nRows, int nCols)
    {
        this.nRows = nRows;
        this.nCols = nCols;
    }
    
    public void xlsParse(List<XLSRecord> records)
    {
        cells = new LinkedHashMap();
        for (int i = 0; i < nRows; ++i)
        {
            for (int j = 0; j < nCols; ++j)
            {
                if (records.get(i).get(j).length() > 0)
                {
                    cells.put(new Index2D(nRows - i - 1, j), records.get(i).get(j));
                }
            }
        }
    }

    @Override
    public Object output() {
        Map<String, Object> out = new LinkedHashMap();
        
        cells.entrySet().stream().forEach((entry) -> {
            out.put(entry.getKey().toString(),
                    this.cellConfigFromString(entry.getValue()));
        });
        
        return out;
    }
    
    private Object cellConfigFromString(String s)
    {
        Map<String, Object> conf = new HashMap(1);
        conf.put("type", s);
        
        return conf;
    }
    
    public static class Index2D
    {
        int row;
        int col;

        public Index2D(int row, int col) {
            this.row = row;
            this.col = col;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 97 * hash + this.row;
            hash = 97 * hash + this.col;
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Index2D other = (Index2D) obj;
            if (this.row != other.row) {
                return false;
            }
            if (this.col != other.col) {
                return false;
            }
            return true;
        }
        
        @Override
        public String toString()
        {
            return String.format("{\"row\":%d,\"col\":%d}", row, col);
        }
    }
}
