/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filltree;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import xlsParser.*;

/**
 *
 * @author Salm
 */
public class FTConfigurator extends xlsconfig.AbstractXLSConfigurator {
    private static final Gson jsonParser;
    
    static
    {
        jsonParser = new GsonBuilder().setPrettyPrinting()
                .create();
    }
    
    @Override
    public void runConfig() throws Exception {
        String gameFile = getInPath() + "\\Game.xls";
        Game game = XLSParser.parse(Game.class, XLSReader.read(gameFile));
        
        String levelFile = getInPath() + "\\Levels.xls";
        XLSWorkbook levelWB = XLSReader.read(levelFile);
        game.Levels.stream().forEach((level) -> {
            try {
                LevelBoard board = new LevelBoard(level.Board.boardsize.row, level.Board.boardsize.col);
                XLSParser.parse(board, LevelBoard.class,
                        levelWB.getSheet(level.Board.boardSheet).getRecords());
                
                level.Board.cells = board.output();
            } catch (Exception ex) {
                Logger.getLogger(FTConfigurator.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
        String outFile = getOutPath() + "\\Game.data";
        Writer writer = new BufferedWriter(new FileWriter(outFile));
        jsonParser.toJson(game, writer);
        writer.flush();
    }
}
